import controller.CityClient;
import controller.NodesClient;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.UUID;
import java.util.logging.Logger;


public class Client {
    static { System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$-7s] %5$s %n"); }
    private static final Logger LOGGER = Logger.getLogger("one");

    private static URI uri;
    private static String in;

    public static void main(String[] args) {
        LOGGER.info("Current Locale: " + Locale.getDefault());
        ResourceBundle messages = ResourceBundle.getBundle("messages");
        Scanner stdin = new Scanner(System.in);

        try {
            uri = new URI("http://localhost:8080");
        } catch (URISyntaxException e) {
            LOGGER.severe(e.toString());
        }

        System.out.println(messages.getString("welcome"));
        System.out.println(messages.getString("menu"));

        do {
            System.out.println(messages.getString("action"));
            in = stdin.nextLine();
            switch (in) {
                case "0":
                    System.out.println(messages.getString("bye"));
                    break;
                case "1":
                    try {
                        CityClient cityClient = new CityClient(uri);
                        cityClient.getCityState();
                    } catch (Exception e) {
                        LOGGER.warning(e.toString());
                        System.out.println(messages.getString("server_not_found"));
                    }
                    break;
                case "2":
                    try {
                        CityClient cityClient = new CityClient(uri);
                        System.out.println(messages.getString("how_many"));
                        int n = Integer.parseInt(stdin.nextLine());
                        cityClient.getCityMeasurements(n);
                    } catch (NumberFormatException e) {
                        LOGGER.warning(e.toString());
                        System.out.println(messages.getString("number_not_valid"));
                    }
                    break;
                case "3":
                    try {
                        CityClient cityClient = new CityClient(uri);
                        System.out.println(messages.getString("what_type"));
                        String type = stdin.nextLine();
                        System.out.println(messages.getString("how_many"));
                        int n = Integer.parseInt(stdin.nextLine());
                        cityClient.getCityMeasurements(type, n);
                    } catch (NumberFormatException e) {
                        LOGGER.warning(e.toString());
                        System.out.println(messages.getString("number_not_valid"));
                    }
                    break;
                case "4":
                    try {
                        NodesClient nodesClient = new NodesClient(uri);
                        System.out.println(messages.getString("how_many"));
                        int n = Integer.parseInt(stdin.nextLine());
                        nodesClient.getLocalMeasurements(n);
                    } catch (NumberFormatException e) {
                        LOGGER.warning(e.toString());
                        System.out.println(messages.getString("number_not_valid"));
                    }
                    break;
                case "5":
                    try {
                        NodesClient nodesClient = new NodesClient(uri);
                        System.out.println(messages.getString("uuid"));
                        UUID uuid = UUID.fromString(stdin.nextLine());
                        System.out.println(messages.getString("how_many"));
                        int n = Integer.parseInt(stdin.nextLine());
                        nodesClient.getNodeMeasurements(uuid, n);
                    } catch (IllegalArgumentException e) {
                        LOGGER.warning(e.toString());
                        System.out.println(messages.getString("uuid_not_valid"));
                    }
                    break;
                case "6":
                    try {
                        CityClient cityClient = new CityClient(uri);
                        System.out.println(messages.getString("how_many"));
                        int n = Integer.parseInt(stdin.nextLine());
                        cityClient.getCityMeans(n);
                    } catch (NumberFormatException e) {
                        LOGGER.warning(e.toString());
                        System.out.println(messages.getString("uuid_not_valid"));
                    }
                    break;
                case "7":
                    try {
                        NodesClient nodesClient = new NodesClient(uri);
                        System.out.println(messages.getString("uuid"));
                        UUID uuid = UUID.fromString(stdin.nextLine());
                        System.out.println(messages.getString("how_many"));
                        int n = Integer.parseInt(stdin.nextLine());
                        nodesClient.getNodeMeans(uuid, n);
                    } catch (IllegalArgumentException e) {
                        LOGGER.warning(e.toString());
                        System.out.println(messages.getString("uuid_not_valid"));
                    }
                    break;
                case "8":
                    try {
                        CityClient cityClient = new CityClient(uri);
                        System.out.println(messages.getString("how_many"));
                        int n = Integer.parseInt(stdin.nextLine());
                        cityClient.getCityStandardDeviations(n);
                    } catch (NumberFormatException e) {
                        LOGGER.warning(e.toString());
                        System.out.println(messages.getString("uuid_not_valid"));
                    }
                    break;
                case "9":
                    try {
                        NodesClient nodesClient = new NodesClient(uri);
                        System.out.println(messages.getString("uuid"));
                        UUID uuid = UUID.fromString(stdin.nextLine());
                        System.out.println(messages.getString("how_many"));
                        int n = Integer.parseInt(stdin.nextLine());
                        nodesClient.getNodeStandardDeviations(uuid, n);
                    } catch (IllegalArgumentException e) {
                        LOGGER.warning(e.toString());
                        System.out.println(messages.getString("uuid_not_valid"));
                    }
                    break;
                case "h":
                    System.out.println(messages.getString("menu"));
                    break;
                default:
                    System.out.println(messages.getString("action_not_valid"));
                    break;
            }
        } while (!in.equals("0"));
    }
}
