package controller;

import utils.TablePrinter;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class NodesClient {
    private WebTarget webTarget;
    private static final String nodes = "/nodes";
    private static final String TYPE = "type";
    private static final String ALL = "all";
    private static final String COUNT = "n";

    public NodesClient(URI uri) {
        webTarget = ClientBuilder.newClient().target(uri);
    }

    public void getLocalMeasurements(int n) {
        Response response = webTarget
                .path(nodes + "/measurements")
                .queryParam("n", n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        Map<UUID, List<Map.Entry<Long, Measurement>>> measurements =
                response.readEntity(new GenericType<Map<UUID, List<Map.Entry<Long, Measurement>>>>(){});
        TablePrinter.printLocalMeasurements(measurements);
    }

    public void getAllTypesLocalMeasurements(int n) {
        Response response = webTarget
                .path(nodes + "/measurements")
                .queryParam(COUNT, n)
                .queryParam(TYPE, ALL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        Map<UUID, Map<String, List<Map.Entry<Long, Measurement>>>> measurements =
                response.readEntity(new GenericType<Map<UUID, Map<String, List<Map.Entry<Long, Measurement>>>>>(){});
        TablePrinter.printAllTypesLocalMeasurements(measurements);
    }

    public void getNodeMeasurements(UUID uuid, int n) {
        Response response = webTarget
                .path(nodes + "/" + uuid.toString() + "/measurements")
                .queryParam("n", n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        List<Map.Entry<Long, Measurement>> measurements =
                response.readEntity(new GenericType<List<Map.Entry<Long, Measurement>>>(){});
        TablePrinter.printNodeMeasurement(measurements, uuid);
    }

    public void getAllTypesNodeMeasurements(UUID uuid, int n) {
        Response response = webTarget
                .path(nodes + "/" + uuid.toString() + "/measurements")
                .queryParam(COUNT, n)
                .queryParam(ALL, n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Map<String, List<Map.Entry<Long, Measurement>>> measurements =
                response.readEntity(new GenericType<Map<String, List<Map.Entry<Long, Measurement>>>>(){});
        TablePrinter.printAllTypesNodeMeasurements(measurements, uuid);
    }

    public void getNodeMeans(UUID uuid, int n) {
        Response response = webTarget
                .path(nodes + "/" + uuid.toString() + "/means")
                .queryParam("n", n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Map<String, Double> means = response.readEntity(new GenericType<Map<String, Double>>(){});
        TablePrinter.printNodeMeans(means, uuid);
    }

    public void getNodeStandardDeviations(UUID uuid, int n) {
        Response response = webTarget
                .path(nodes + "/" + uuid.toString() + "/stdevs")
                .queryParam(COUNT, n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Map<String, Double> stdevs = response.readEntity(new GenericType<Map<String, Double>>(){});
        TablePrinter.printNodeStdevs(stdevs, uuid);
    }

    public void getNodeMeans(UUID uuid, String type, int n) {
        Response response = webTarget
                .path(nodes + "/" + uuid.toString() + "/means")
                .queryParam(TYPE, type)
                .queryParam(COUNT, n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Map<String, Double> means = response.readEntity(new GenericType<Map<String, Double>>(){});
        TablePrinter.printNodeMeans(means, uuid);
    }

    public void getNodeStandardDeviations(UUID uuid, String type, int n) {
        Response response = webTarget
                .path(nodes + "/" + uuid.toString() + "/stdevs")
                .queryParam(TYPE, type)
                .queryParam(COUNT, n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Map<String, Double> stdevs = response.readEntity(new GenericType<Map<String, Double>>(){});
        TablePrinter.printNodeStdevs(stdevs, uuid);
    }


}
