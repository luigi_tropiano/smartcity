package controller;

import utils.TablePrinter;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Produces("application/json")
@Consumes(MediaType.APPLICATION_JSON)
public class CityClient {
    private WebTarget webTarget;
    private static final String CITY = "/city";
    private static final String TYPE = "type";
    private static final String COUNT = "n";

    public CityClient(URI uri) {
        webTarget = ClientBuilder.newClient().target(uri);
    }

    public void getCityState() {
        Response response = webTarget
                .path(CITY + "/state")
                .request(MediaType.APPLICATION_JSON)
                .get();

        //HashMap map = response.readEntity(HashMap.class);
        Map<UUID, Point> cityState = response.readEntity(new GenericType<HashMap<UUID, Point>>(){});
        TablePrinter.printCityState(cityState);
    }

    public void getCityMeasurements(int n) {
        Response response = webTarget
                .path(CITY + "/measurements")
                .queryParam(COUNT, n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        List<Map.Entry<Long, Measurement>> measurements =
                response.readEntity(new GenericType<List<Map.Entry<Long, Measurement>>>(){});
        TablePrinter.printCityMeasurements(measurements);
    }

    public void getCityMeasurements(String type, int n) {
        Response response = webTarget
                .path(CITY + "/measurements")
                .queryParam(TYPE, type)
                .queryParam(COUNT, n)
                .request(MediaType.APPLICATION_JSON)
                .get();

        List<Map.Entry<Long, Measurement>> measurements =
                response.readEntity(new GenericType<List<Map.Entry<Long, Measurement>>>(){});
        TablePrinter.printCityMeasurements(measurements);
    }

    public void getCityMean(String type, int n) {
        Response response = webTarget
                .path(CITY + "/means")
                .queryParam(TYPE, type)
                .queryParam(COUNT, n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        Double mean = response.readEntity(new GenericType<Double>(){});
        TablePrinter.printCityMean(type, mean);
    }

    public void getCityStandardDeviation(String type, int n) {
        Response response = webTarget
                .path(CITY + "/stdevs")
                .queryParam(TYPE,  type)
                .queryParam(COUNT, n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        Double mean = response.readEntity(new GenericType<Double>(){});
        TablePrinter.printCityStandardDeviation(type, mean);
    }

    public void getCityMeans(int n) {
        Response response = webTarget
                .path(CITY + "/means")
                .queryParam(COUNT, n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        Map<String, Double> means = response.readEntity(new GenericType<Map<String, Double>>(){});
        TablePrinter.printCityMeans(means);
    }

    public void getCityStandardDeviations(int n) {
        Response response = webTarget
                .path(CITY + "/stdevs")
                .queryParam(COUNT, n)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        Map<String, Double> stdevs = response.readEntity(new GenericType<Map<String, Double>>() {
        });
        TablePrinter.printCityStandardDeviations(stdevs);
    }
}

