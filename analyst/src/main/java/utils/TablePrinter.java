package utils;

import controller.Measurement;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestLine;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;

import java.awt.*;
import java.sql.Timestamp;
import java.util.*;
import java.util.List;

public final class TablePrinter {

    private TablePrinter(){}

    public static void printCityState(Map<UUID, Point> response){
        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow("UUID", "X", "Y");
        at.addRule();
        response.forEach((uuid, point) -> {
            at.addRow(uuid, point.x, point.y);
            at.addRule();
        });
        renderTable(at);
    }

    public static void printCityMeasurements(List<Map.Entry<Long, Measurement>> measurements){
        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow("Timestamp", "UUID", "Type", "Value");
        at.addRule();
        measurements.forEach((entry) -> {
            at.addRow(
                    new Timestamp(entry.getKey()),
                    entry.getValue().getUuid(),
                    entry.getValue().getType(),
                    entry.getValue().getValue()
            );
            at.addRule();
        });
        renderTable(at);
    }

    public static void printNodeMeasurement(List<Map.Entry<Long, Measurement>> measurements, UUID uuid) {
        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow("UUID", null, uuid);
        at.addRule();
        at.addRow("Timestamp", "Type", "Value");
        at.addRule();
        measurements.forEach((entry) -> {
            at.addRow(
                    new Timestamp(entry.getKey()),
                    entry.getValue().getType(),
                    entry.getValue().getValue()
            );
            at.addRule();
        });
        renderTable(at);
    }

    public static void printLocalMeasurements(Map<UUID, List<Map.Entry<Long, Measurement>>> measurements) {

        measurements.forEach((uuid, entries) -> {

            AsciiTable at = new AsciiTable();
            at.addRule();
            at.addRow("UUID", null, uuid.toString());
            at.addRule();
            at.addRow("Timestamp", "Type", "Value");
            at.addRule();
            entries.forEach(entry -> {
                at.addRow(
                        new Timestamp(entry.getKey()),
                        entry.getValue().getType(),
                        entry.getValue().getValue()
                );
                at.addRule();
            });
            renderTable(at);
        });
    }

    public static void printAllTypesLocalMeasurements(Map<UUID, Map<String, List<Map.Entry<Long, Measurement>>>> measurements) {

        measurements.forEach((uuid, entries) -> {
            AsciiTable at = new AsciiTable();
            at.addRule();
            at.addRow("UUID", uuid.toString());
            at.addRule();

            entries.forEach((type, list) -> {
                at.addRow("Type", uuid.toString());
                at.addRule();
                list.forEach(entry -> {
                    at.addRow("Timestamp", "Value");
                    at.addRule();
                    at.addRow(
                            new Timestamp(entry.getKey()),
                            entry.getValue().getType(),
                            entry.getValue().getValue()
                    );
                    at.addRule();
                });
            });
            renderTable(at);
        });
    }

    public static void printAllTypesNodeMeasurements(Map<String, List<Map.Entry<Long, Measurement>>> measurements, UUID uuid) {

        AsciiTable at = new AsciiTable();
        at.addRule();
        at.addRow("UUID", uuid.toString());
        at.addRule();

        measurements.forEach((type, list) -> {
            at.addRow("Type", uuid.toString());
            at.addRule();
            list.forEach(entry -> {
                at.addRow("Timestamp", "Value");
                at.addRule();
                at.addRow(
                        new Timestamp(entry.getKey()),
                        entry.getValue().getType(),
                        entry.getValue().getValue()
                );
                at.addRule();
            });
        });
        renderTable(at);
    }

    public static void printCityMean(String type, Double mean){
        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow("Type", "Value");
        at.addRule();
        at.addRow(type, mean);
        at.addRule();

        renderTable(at);
    }

    public static void printCityStandardDeviation(String type, Double stdev){
        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow("Type", "Value");
        at.addRule();
        at.addRow(type, stdev);
        at.addRule();

        renderTable(at);
    }

    public static void printCityMeans(Map<String, Double> means) {
        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow(null, "GLOBAL MEANS");
        at.addRule();
        at.addRow("Type", "Value");
        at.addRule();
        means.forEach((type, value) -> {
            at.addRow(type, value);
            at.addRule();
        });
        renderTable(at);
    }

    public static void printCityStandardDeviations(Map<String, Double> stdevs) {
        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow(null, "GLOBAL STDEVS");
        at.addRule();
        at.addRow("Type", "Value");
        at.addRule();
        stdevs.forEach((type, value) -> {
            at.addRow(type, value);
            at.addRule();
        });
        renderTable(at);
    }

    public static void printNodeMeans(Map<String, Double> means, UUID uuid){
        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow(null, "Means of " + uuid);
        at.addRule();
        at.addRow("Type", "Value");
        at.addRule();
        means.forEach((type, value) -> {
            at.addRow(type, value);
            at.addRule();
        });
        renderTable(at);
    }

    public static void printNodeStdevs(Map<String, Double> means, UUID uuid){
        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow(null, "Stdevs of " + uuid);
        at.addRule();
        at.addRow("Type", "Value");
        at.addRule();
        means.forEach((type, value) -> {
            at.addRow(type, value);
            at.addRule();
        });
        renderTable(at);
    }

    private static void renderTable(AsciiTable table){
        CWC_LongestLine cwc = new CWC_LongestLine();
        table.setPaddingLeft(2);
        table.setPaddingRight(2);
        table.setTextAlignment(TextAlignment.CENTER);
        table.getRenderer().setCWC(cwc);
        System.out.println(table.render());

    }

}
