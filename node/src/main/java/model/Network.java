package model;

import java.awt.*;
import java.util.*;

import static java.lang.Math.abs;

public class Network {
    private Node root;
    private Node parent;
    private long latestEdit;
    private final Node me;
    private final Map<UUID, Node> nodeMap;
    private final Map<UUID, Set<UUID>> tree;

    public Network(Node me, Map<UUID, Node> nodeMap){
        this.me = me;
        this.root = null;
        this.parent = null;
        this.nodeMap = nodeMap;
        this.tree = new HashMap<>();
        this.latestEdit = System.currentTimeMillis();
    }

    public synchronized int size(){
        return nodeMap.size();
    }

    public synchronized boolean amIRoot(){
        if (root == null)
            return false;
        return me.getUuid().equals(root.getUuid());
    }

    public synchronized boolean amIParent(){
        if (root == null)
            return false;
        if (parent == null)
            return false;
        return parent.getUuid().equals(root.getUuid());
    }

    public synchronized Map<UUID, Node> getCopy(){
        return new HashMap<>(nodeMap);
    }

    public synchronized Node getNode(UUID uuid){
        return nodeMap.get(uuid);
    }

    public synchronized Set<UUID> getNodeChildren (UUID father){
        return tree.get(father);
    }

    public synchronized Node getRoot(){
        return root;
    }

    public synchronized boolean setRoot(UUID root){
        if (!nodeMap.containsKey(root))
            return false;
        this.root = nodeMap.get(root);
        return true;
    }

    public synchronized Node getMyParent(){
        return parent;
    }

    public synchronized boolean setMyParent(UUID parent){
        if (!nodeMap.containsKey(parent))
            return false;
        this.parent = nodeMap.get(parent);
        return true;
    }

    public synchronized void addNode(Node node){
        nodeMap.put(node.getUuid(), node);
        if (amIRoot()) addToTree(node.getUuid());
    }

    public synchronized void removeNode(UUID uuid){
        nodeMap.remove(uuid);

        Set<UUID> set = tree.remove(uuid);
        if (set == null)
            tree.forEach((k, v) -> v.remove(uuid));
        else if (!set.isEmpty()) {
            UUID newParent = set.iterator().next();
            set.remove(newParent);
            tree.put(newParent, set);
        }
    }

    public synchronized boolean replaceParent(UUID newParent) {
        for (Map.Entry<UUID, Set<UUID>> entry : tree.entrySet()) {
            if (entry.getValue().contains(newParent)) {
                Set<UUID> set = tree.remove(entry.getKey());
                set.remove(newParent);
                tree.put(newParent, set);
                return true;
            }
        }
        return false;
    }

    public synchronized UUID getPeerParent(UUID uuid){
        if (tree.containsKey(uuid))
            return root.getUuid();
        else
            return tree
                    .entrySet()
                    .stream()
                    .filter(entry -> entry.getValue().contains(uuid))
                    .map(Map.Entry::getKey)
                    .findAny()
                    .orElse(null);
    }

    public synchronized void rebuildTree(){
        this.tree.clear();
        nodeMap.keySet().stream().filter(uuid -> !uuid.equals(me.getUuid())).forEach(this::addToTree);
    }

    public synchronized long getLatestEdit() {
        return latestEdit;
    }

    public synchronized Node getNearestNode(Point position) {

        Node nearest = null;
        double minDistance = Double.POSITIVE_INFINITY;

        for (Set<UUID> set : tree.values())
            for (UUID uuid : set) {
                double distance = getDistance(position, nodeMap.get(uuid).getPosition());
                if (distance <= minDistance) {
                    minDistance = distance;
                    nearest = nodeMap.get(uuid);
                }
            }
        return nearest;
    }

    public synchronized void updateLatestEdit() {
        this.latestEdit = System.currentTimeMillis();
    }

    private synchronized double getDistance(Point firstPoint, Point secondPoint){
        return (abs(firstPoint.x - secondPoint.x) + abs(firstPoint.y - secondPoint.y));
    }

    private synchronized void addToTree(UUID uuid){
        for (Set<UUID> set : tree.values()) {
            if (set.size() <= tree.size()) {
                set.add(uuid);
                return;
            }
        }
        tree.put(uuid, new HashSet<>());
    }

}
