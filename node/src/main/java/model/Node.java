package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.awt.*;
import java.net.URI;
import java.util.Objects;
import java.util.UUID;

public class Node {
    @JsonProperty ("uuid") private UUID uuid;
    @JsonProperty ("position") private Point position;
    @JsonProperty ("address") private URI address;
    @JsonProperty ("federationPort") private Integer federationPort;
    @JsonProperty ("analyticsPort") private Integer analyticsPort;

    @JsonCreator
    public Node(
            @JsonProperty("uuid") UUID uuid,
            @JsonProperty("position") Point position,
            @JsonProperty("address") URI address,
            @JsonProperty("federationPort") int federationPort,
            @JsonProperty("analyticsPort") int analyticsPort) {
        this.uuid = uuid;
        this.position = position;
        this.address = address;
        this.analyticsPort = analyticsPort;
        this.federationPort = federationPort;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Point getPosition() {
        return new Point(position);
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public URI getAddress() {
        return address;
    }

    public void setAddress(URI address) {
        this.address = address;
    }

    public Integer getFederationPort() {
        return federationPort;
    }

    public void setFederationPort(Integer federationPort) {
        this.federationPort = federationPort;
    }

    public Integer getAnalyticsPort() {
        return analyticsPort;
    }

    public void setAnalyticsPort(Integer analyticsPort) {
        this.analyticsPort = analyticsPort;
    }

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        Node node = (Node) o;
        // field comparison
        return Objects.equals(uuid, node.uuid)
                && Objects.equals(address, node.address)
                && Objects.equals(position, node.position)
                && Objects.equals(federationPort, node.federationPort)
                && Objects.equals(analyticsPort, node.analyticsPort);
    }
}
