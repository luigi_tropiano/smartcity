package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.UUID;

public class WelcomeMessage {
    private Map<UUID, Node> network;

    @JsonCreator
    public WelcomeMessage(
            @JsonProperty ("network") Map<UUID, Node> network){
        this.network = network;
    }

    public Map<UUID, Node> getNetwork() {
        return network;
    }
}
