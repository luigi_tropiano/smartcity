package model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Measurement {

    @JsonProperty("uuid") private final UUID uuid;
    @JsonProperty("type") private final String type;
    @JsonProperty("value") private final double value;
    @JsonProperty("timestamp") private final long timestamp;

    public Measurement(UUID uuid, String type, double value, long timestamp) {
        this.uuid = uuid;
        this.value = value;
        this.type = type;
        this.timestamp = timestamp;
    }

    public UUID getUuid() {
        return uuid;
    }

    public double getValue() {
        return value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getType() {
        return type;
    }

    public String toString(){
        return value + " " + timestamp;
    }
}
