package model;

import it.unimi.sdp.FederationServiceOuterClass.MeanMessage;
import sync.Syncelection;
import sync.Syncstate;

import java.net.URI;
import java.util.*;

public class Kernel {
    //networking
    private final Node me;
    private final URI serverAddress;
    private final Network network;
    // measurements
    private final Map<String, Queue<Measurement>> measurementsMap;
    private final Map<String, Queue<Measurement>> meansMap;
    private final Set<Measurement> rawMeansSet;
    private final Map<String, List<Measurement>> localHistoryMap;
    private final Map<String, MeanMessage> latestGlobalMeasurements;
    //sync
    private final Syncelection syncGlobalElection;
    private final Syncelection syncLocalElection;
    private final Syncstate globalState;
    private final Syncstate localState;
    //other
    private final Set<Thread> openThreads;

    public Kernel(Node me, Map<UUID, Node> network, URI serverAddress){
        //networking
        this.me = me;
        this.serverAddress = serverAddress;
        this.network = new Network(me, network);
        // measurements
        this.measurementsMap = new HashMap<>();
        this.meansMap = new HashMap<>();
        this.rawMeansSet = new HashSet<>();
        this.localHistoryMap = new HashMap<>();
        this.latestGlobalMeasurements = new HashMap<>();
        //sync
        this.globalState = new Syncstate();
        this.localState = new Syncstate();
        this.syncGlobalElection = new Syncelection();
        this.syncLocalElection = new Syncelection();
        //other
        this.openThreads = new HashSet<>();

    }

    public Syncstate getGlobalState() {
        return globalState;
    }

    public Syncstate getLocalState() {
        return localState;
    }

    public Syncelection getSyncGlobalElection() {
        return syncGlobalElection;
    }

    public Syncelection getSyncLocalElection() {
        return syncLocalElection;
    }

    public URI getServerAddress() {
        return serverAddress;
    }

    public Node getMe() {
        return me;
    }

    public Network getNetwork() {
        return network;
    }

    public Map<String, Queue<Measurement>> getMeasurementsMap() {
        return measurementsMap;
    }

    public Map<String, Queue<Measurement>> getMeansMap() {
        return meansMap;
    }

    public Set<Measurement> getRawMeansSet() {
        return rawMeansSet;
    }

    public synchronized Thread registerThread(Thread thread) {
        this.openThreads.add(thread);
        return thread;
    }

    public synchronized void stopAllThreads() {
        openThreads.forEach(Thread::interrupt);
    }

    public Map<String, List<Measurement>> getLocalHistoryMap() {
        return localHistoryMap;
    }

    public Map<String, MeanMessage> getLatestGlobalMeasurements() {
        return latestGlobalMeasurements;
    }
}
