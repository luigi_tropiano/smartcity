import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import communication.RestClient;
import controller.NodeThread;
import model.Kernel;
import model.Node;
import model.WelcomeMessage;
import utils.Args;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.URI;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class StartNode {
    static { System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$-7s] %5$s %n"); }
    private static final Logger LOGGER = Logger.getLogger("node");

    public static void main(String[] argv) {
        Node node;
        URI serverAddress;
        RestClient restClient;
        Args args = new Args();

        try {

            JCommander.newBuilder().addObject(args).build().parse(argv);

            Level logLevel = args.getLogLevel();
            if (logLevel == null)
                Arrays.stream(LogManager.getLogManager().getLogger("").getHandlers()).forEach(h -> h.setLevel(Level.WARNING));
            else
                Arrays.stream(LogManager.getLogManager().getLogger("").getHandlers()).forEach(h -> h.setLevel(logLevel));

            UUID uuid = args.getUuid();
            Integer federationPort = args.getFederation();
            Integer analyticsPort = args.getAnalytics();
            URI address = URI.create("localhost");
            serverAddress = args.getServerAddress();

            if (uuid == null) {
                uuid = UUID.randomUUID();
                LOGGER.warning("[BOOT] -> using random uuid [" + uuid + "]");
            }
            if (federationPort == null) {
                federationPort = randomPort();
                LOGGER.warning("[BOOT] -> using random federation port :: " + federationPort);
            }
            if (analyticsPort == null) {
                analyticsPort = randomPort();
                LOGGER.warning("[BOOT] -> using random analytics port :: " + analyticsPort);
            }
            if (serverAddress == null) {
                serverAddress = URI.create("http://localhost:8080");
                LOGGER.warning("[BOOT] -> using default server address :: " + serverAddress);
            }

            if (!isPortAvailable(address.getHost(), federationPort)) {
                LOGGER.severe("[BOOT] -> local port not available :: " + federationPort);
                return;
            }
            if (!isPortAvailable(address.getHost(), analyticsPort)) {
                LOGGER.severe("[BOOT] -> local port not available :: " + federationPort);
                return;
            }

            node = new Node(uuid, randomPosition(), address, federationPort, analyticsPort);
            restClient = new RestClient(serverAddress);

            for (int i = 0; i < 10; i++) {
                node.setPosition(randomPosition());
                LOGGER.warning("[BOOT] -> sending add request for " + node.getPosition().toString());

                Response response = restClient.addNode(node);

                switch (response.getStatus()) {
                    case 200: // OK
                        WelcomeMessage message = response.readEntity(new GenericType<WelcomeMessage>() {});
                        Map<UUID, Node> network = message.getNetwork();

                        LOGGER.info("[SERVER] -> network size is " + network.size());
                        LOGGER.info("[BOOT] -> booting");

                        NodeThread nodeThread = new NodeThread(new Kernel(node, network, serverAddress));
                        nodeThread.start();
                        nodeThread.join();

                        LOGGER.info("[SHUTDOWN] -> goodbye");
                        return;
                    case 204: // NO CONTENT
                        LOGGER.severe("[SERVER] -> NO CONTENT");
                        return;
                    case 400: // BAD REQUEST
                        LOGGER.severe("[SERVER] -> BAD REQUEST");
                        return;
                    case 409:
                        LOGGER.warning("[SERVER] -> UUID already taken or position not available");
                        Thread.sleep(1000);
                        break;
                    case 500:
                        LOGGER.severe("[SERVER] -> INTERNAL ERROR");
                        return;
                    default:
                        LOGGER.warning(response.getStatusInfo().toString());
                        return;
                }
            }

        } catch (ParameterException e) {
            //e.getJCommander().setProgramName("");
            LOGGER.severe(e.getMessage());
            e.usage();
        } catch (ProcessingException | InterruptedException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    private static boolean isPortAvailable(String host, int port) {
        // Assume port is available.
        boolean result = true;

        try {
            (new Socket(host, port)).close();
            // Successful connection means the port is taken.
            result = false;
        } catch(SocketException e) {
            // could not connect -> port available
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
        return result;
    }

    private static int randomPort(){
        return (1024 + (int)(Math.random() * ((65535 - 1024) + 1)));
    }

    private static Point randomPosition(){
        return new Point((int)(Math.random() * 100), (int)(Math.random() * 100));
    }

}