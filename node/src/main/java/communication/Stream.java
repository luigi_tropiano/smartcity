package communication;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import it.unimi.sdp.FederationServiceGrpc;
import it.unimi.sdp.FederationServiceGrpc.FederationServiceStub;
import model.Kernel;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static it.unimi.sdp.FederationServiceOuterClass.GlobalMeans;
import static it.unimi.sdp.FederationServiceOuterClass.MeanMessage;

public class Stream {
    private static final Logger LOGGER = Logger.getGlobal();

    private final Kernel kernel;
    private final ManagedChannel channel;
    private final FederationServiceStub asyncStub;
    private StreamObserver<MeanMessage> meanStreamObserver;
    private StreamObserver<MeanMessage> rawMeanStreamObserver;

    public Stream(Kernel kernel) {
        this.kernel = kernel;
        channel = ManagedChannelBuilder.forAddress(
                kernel.getNetwork().getMyParent().getAddress().toString(),
                kernel.getNetwork().getMyParent().getFederationPort()
        ).usePlaintext().build();
        asyncStub = FederationServiceGrpc.newStub(channel);
        meanStreamObserver = meanStreamObserverBuilder();
        rawMeanStreamObserver = rawMeanStreamObserverBuilder();


    }

    private StreamObserver<MeanMessage> meanStreamObserverBuilder() {
        return asyncStub.meansStream(new StreamObserver<GlobalMeans>() {

            @Override
            public void onNext(GlobalMeans lastMeans) {
                synchronized (kernel.getLatestGlobalMeasurements()) {
                    lastMeans.getGlobalMeansList().forEach(meanMessage ->
                        kernel.getLatestGlobalMeasurements().put(meanMessage.getType(), meanMessage));
                }
            }

            @Override
            public void onError(Throwable throwable) {
                LOGGER.warning("[stream] -> " + throwable.getMessage());

                synchronized (kernel) {
                    kernel.notify();
                }
            }

            @Override
            public void onCompleted() {
                LOGGER.info("[stream] -> completed");
            }
        });
    }

    private StreamObserver<MeanMessage> rawMeanStreamObserverBuilder() {
        return asyncStub.rawMeansStream(new StreamObserver<MeanMessage>() {

            @Override
            public void onNext(MeanMessage lastMean) {
            }

            @Override
            public void onError(Throwable throwable) {
                LOGGER.warning("[rawStream] -> " + throwable.getMessage());

                synchronized (kernel) {
                    kernel.notify();
                }
            }

            @Override
            public void onCompleted() {
                LOGGER.info("[rawStream] -> completed");
            }

        });
    }

    public void shutdown() {
        try {
            if (channel != null) {
                if (!channel.isShutdown()) {
                    meanStreamObserver.onCompleted();
                    rawMeanStreamObserver.onCompleted();
                    channel.shutdown().awaitTermination(2, TimeUnit.SECONDS);
                }
            }
        } catch (InterruptedException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    public boolean sendMean(MeanMessage message) {

        if (channel.isShutdown())
            return false;

        meanStreamObserver.onNext(message);
        return true;
    }

    public boolean sendRawMeans(MeanMessage message) {

        if (channel.isShutdown())
            return false;

        rawMeanStreamObserver.onNext(message);
        return true;
    }
}
