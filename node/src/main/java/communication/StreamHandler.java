package communication;

import it.unimi.sdp.FederationServiceOuterClass.MeanMessage;
import model.Kernel;
import model.Measurement;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.logging.Logger;


public class StreamHandler {
    private static final Logger LOGGER = Logger.getLogger("node");

    private Stream stream;
    private Kernel kernel;
    private final Queue<MeanMessage> meanCache;
    private final Queue<MeanMessage> rawMeanCache;

    public StreamHandler(Kernel kernel){
        this.kernel = kernel;
        this.meanCache = new ArrayDeque<>();
        this.rawMeanCache = new ArrayDeque<>();
    }

    public void stopStream(){
        if (stream != null) {
            LOGGER.warning("[stream] -> stopping");
            stream.shutdown();
        }
    }

    public void newStream(){
        LOGGER.warning("[stream] -> setting :: " + kernel.getNetwork().getMyParent().getAddress() +
                " @ " + kernel.getNetwork().getMyParent().getFederationPort());
        stream = new Stream(kernel);
        LOGGER.warning("[stream] -> done");
    }

    public void sendMean (Measurement measurement){

        MeanMessage message = MeanMessage.newBuilder()
                .setId(measurement.getUuid().toString())
                .setType(measurement.getType())
                .setValue(measurement.getValue())
                .setTimestamp(measurement.getTimestamp())
                .build();

        //Synchronized (meanCache) {
        //    meanCache.add(message);
        //}

        stream.sendMean(message);
    }

    public void sendRawMeans (Measurement measurement){

        MeanMessage message = MeanMessage.newBuilder()
                .setId(measurement.getUuid().toString())
                .setType(measurement.getType())
                .setValue(measurement.getValue())
                .setTimestamp(measurement.getTimestamp())
                .build();

        //synchronized (rawMeanCache) {
        //    rawMeanCache.add(message);
        //    if (!channel.isShutdown())
        //        rawMeanCache.forEach(rawMean -> requestObserverRawMean.onNext(rawMeanCache.poll()));
        //}

        stream.sendRawMeans(message);
    }

}
