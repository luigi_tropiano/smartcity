package communication;

import model.Measurement;
import model.Node;

import javax.ws.rs.Consumes;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.Produces;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Map;
import java.util.Set;

@Produces("application/json")
@Consumes(MediaType.APPLICATION_JSON)

public class RestClient {
    private WebTarget webTarget;

    public RestClient(URI uri) {
        webTarget = ClientBuilder.newClient().target(uri);
    }

    public Response addNode(Node node) throws ProcessingException {
        return webTarget
                .path("/nodes")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(node));
    }

    public Response removeNode(Node node) throws ProcessingException {
        return webTarget
                .path("/nodes/" + node.getUuid())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .delete();
    }

    public Response sendMeasurements(Set<Measurement> measurementsSet) throws ProcessingException {
        return webTarget
                .path("/nodes/measurements")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(measurementsSet));
    }

    public Response sendGlobalMeasurements(Map<String, Measurement> globalMap) throws ProcessingException {
        return webTarget
                .path("/city/measurements")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(globalMap));
    }
}