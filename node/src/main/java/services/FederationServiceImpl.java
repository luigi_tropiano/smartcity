package services;

import communication.StreamHandler;
import controller.GlobalElectionThread;
import controller.LocalElectionThread;
import io.grpc.stub.StreamObserver;
import it.unimi.sdp.Commons.NodeInfo;
import it.unimi.sdp.Commons.SensorInfo;
import it.unimi.sdp.FederationServiceOuterClass.*;
import model.Kernel;
import model.Measurement;
import model.Network;
import model.Node;
import routines.MeansRoutine;
import routines.RawMeansRoutine;
import utils.CheckPeer;

import java.awt.*;
import java.net.URI;
import java.util.*;
import java.util.Queue;
import java.util.logging.Logger;

import static it.unimi.sdp.FederationServiceGrpc.FederationServiceImplBase;
import static it.unimi.sdp.FederationServiceOuterClass.VictoryResponse.newBuilder;

public class FederationServiceImpl extends FederationServiceImplBase{
    private static final Logger LOGGER = Logger.getLogger("node");
    private final Node me;
    private final Kernel kernel;
    private final Network network;
    private final StreamHandler streamHandler;

    public FederationServiceImpl(Kernel kernel, StreamHandler streamHandler){
        this.kernel = kernel;
        this.me = kernel.getMe();
        this.network = kernel.getNetwork();
        this.streamHandler = streamHandler;

        registerNewThread(new RawMeansRoutine(kernel.getRawMeansSet(), streamHandler));
    }

    @Override
    public void helloRequest(NodeInfo message, StreamObserver<HelloResponse> responseObserver){
        UUID uuid = UUID.fromString(message.getUuid());
        Point position = new Point(message.getPosition().getX(), message.getPosition().getY());
        URI address = URI.create(message.getAddress());
        int federationPort = message.getFederationPort();
        int analyticsPort = message.getAnalyticsPort();

        Node node = new Node(uuid, position, address, federationPort, analyticsPort);

        HelloResponse response;

        synchronized (network){
            network.addNode(node);
            network.updateLatestEdit();

            LOGGER.info("[" + uuid + "] -> add request");
            LOGGER.info("[network] -> size is " + network.size());

            if(network.amIRoot()) {
                response = HelloResponse.newBuilder()
                        .setIsCoordinator(true)
                        .setParent(network.getPeerParent(uuid).toString())
                        .build();
            }
            else
                response = HelloResponse.newBuilder()
                        .setIsCoordinator(false).build();

        }

        responseObserver.onNext(response);
        responseObserver.onCompleted();

    }

    @Override
    public void nearestNode(SensorInfo message, StreamObserver<NodeInfo> responseObserver){

        Point sensorPosition = new Point(
                message.getPosition().getX(),
                message.getPosition().getY()
        );

        LOGGER.info("[nearestNode] -> new request for " + sensorPosition.toString());
        Node nearestNode = network.getNearestNode(sensorPosition);
        while (!CheckPeer.isPeerAlive(nearestNode)){
            network.removeNode(nearestNode.getUuid());
            nearestNode = network.getNearestNode(sensorPosition);
        }
        LOGGER.info("[nearestNode] -> selected node is " + nearestNode.getUuid() + " @ " + nearestNode.getPosition().toString());

        NodeInfo response = NodeInfo.newBuilder()
                .setUuid(nearestNode.getUuid().toString())
                .setAddress(nearestNode.getAddress().toString())
                .setFederationPort(nearestNode.getFederationPort())
                .setAnalyticsPort(nearestNode.getAnalyticsPort())
                .setPosition(NodeInfo.Position.newBuilder()
                        .setX(nearestNode.getPosition().x)
                        .setY(nearestNode.getPosition().y).build())
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void globalElection(ElectionMessage message, StreamObserver<ElectionResponse> responseObserver) {
        LOGGER.warning("[" + message.getUuid() + "] -> global election");

        boolean takeCare = (me.getUuid().compareTo(UUID.fromString(message.getUuid())) > 0);

        ElectionResponse response = ElectionResponse.newBuilder().setTakeCare(takeCare).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

        if (takeCare && !network.amIRoot()) {
            GlobalElectionThread globalElectionThread = new GlobalElectionThread(kernel, streamHandler);
            globalElectionThread.start();
        }
    }

    @Override
    public void globalVictory(ElectionMessage message, StreamObserver<VictoryResponse> responseObserver){
        LOGGER.warning("[" + message.getUuid() + "] -> global victory");

        streamHandler.stopStream();

        UUID newCoordinatorUuid = UUID.fromString(message.getUuid());
        UUID newParentUuid = UUID.fromString(message.getParent());

        VictoryResponse response = newBuilder()
                .setConflict(newCoordinatorUuid.compareTo(me.getUuid()) < 0)
                .build();

        synchronized (kernel.getGlobalState()){
            network.setMyParent(newParentUuid);
            network.setRoot(newCoordinatorUuid);
            network.updateLatestEdit();
            kernel.getGlobalState().unsetCriticalState();
            kernel.getGlobalState().notifyAll();
        }

        LOGGER.warning("[" + newCoordinatorUuid + "] -> new coordinator");
        LOGGER.warning("[" + newParentUuid + "] -> new local parent");

        responseObserver.onNext(response);
        responseObserver.onCompleted();

        //streamHandler.newStream();
        new Thread(streamHandler::newStream).start();
    }

    @Override
    public void localElection(ElectionMessage message, StreamObserver<ElectionResponse> responseObserver){
        LOGGER.warning("[" + message.getUuid() + "] -> local election");

        UUID uuid = UUID.fromString(message.getUuid());

        boolean takeCare = ( uuid.equals(network.getMyParent().getUuid())
                && me.getUuid().compareTo(UUID.fromString(message.getUuid())) > 0 ) ;

        ElectionResponse response = ElectionResponse.newBuilder().setTakeCare(takeCare).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

        if (takeCare && !network.amIParent()){
            LocalElectionThread localElectionThread = new LocalElectionThread(kernel, streamHandler);
            localElectionThread.start();
        }
    }

    @Override
    public void localVictory(ElectionMessage message, StreamObserver<VictoryResponse> responseObserver) {
        LOGGER.warning("[" + message.getUuid() + "] -> local victory");

        UUID oldParentUuid = UUID.fromString(message.getParent());
        UUID newParentUuid = UUID.fromString(message.getUuid());

        if (oldParentUuid.equals(network.getMyParent().getUuid())) {

            streamHandler.stopStream();

            VictoryResponse response = newBuilder()
                    .setConflict(newParentUuid.compareTo(me.getUuid()) < 0)
                    .build();

            synchronized (kernel.getLocalState()) {
                network.setMyParent(newParentUuid);
                network.updateLatestEdit();
                kernel.getLocalState().unsetCriticalState();
                kernel.getLocalState().notifyAll();
            }

            LOGGER.warning("[" + newParentUuid + "] -> new local parent");

            responseObserver.onNext(response);
            responseObserver.onCompleted();

            //streamHandler.startStreams();
            new Thread(streamHandler::newStream).start();
        }
    }

    @Override
    public void getCompanions(ElectionMessage message, StreamObserver<CompanionsList> responseObserver) {

        UUID uuid = UUID.fromString(message.getUuid());
        HashSet<String> set = new HashSet<>();
        network.getNodeChildren(uuid).forEach(id -> set.add(id.toString()));

        CompanionsList response = CompanionsList.newBuilder().addAllId(set).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void newLocalParent(NodeInfo message, StreamObserver<VictoryResponse> responseObserver) {

        UUID uuid = UUID.fromString(message.getUuid());
        boolean conflict = (!network.replaceParent(uuid));

        VictoryResponse response = VictoryResponse.newBuilder().setConflict(conflict).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

    }

    @Override
    public void isPeerAlive(Ping ping, StreamObserver<Ping> responseObserver){

        Ping pong = Ping.newBuilder().build();
        responseObserver.onNext(pong);
        responseObserver.onCompleted();
    }

    // analytics
    @Override
    public StreamObserver<MeanMessage> meansStream(final StreamObserver<GlobalMeans> responseObserver){

        LOGGER.info("[meansStreamService] -> new incoming connection");

        return new StreamObserver<MeanMessage>(){

            @Override
            public void onNext(MeanMessage messageIn){

                UUID uuid = UUID.fromString(messageIn.getId());
                String type = messageIn.getType();
                double value = messageIn.getValue();
                long timestamp = messageIn.getTimestamp();

                Measurement newMeasurement = new Measurement(uuid, type, value, timestamp);

                Map<String, Queue<Measurement>> meansMap = kernel.getMeansMap();
                // if type is a new type, create a new handler thread for that type
                synchronized (meansMap) {
                    if (meansMap.putIfAbsent(type, new ArrayDeque<>()) == null)
                        registerNewThread(new MeansRoutine(type, kernel, streamHandler));
                }

                Set<Measurement> rawMeansQueue = kernel.getRawMeansSet();
                Queue<Measurement> queue = kernel.getMeansMap().get(type);

                synchronized (rawMeansQueue) {
                    rawMeansQueue.add(newMeasurement);
                }

                synchronized (queue) {
                    queue.add(newMeasurement);
                    LOGGER.info("[meansStreamService] -> mean [" + queue.size() + "] received from " + uuid);
                }


                if ( ! network.amIRoot()) {

                    synchronized (rawMeansQueue) {
                        if (rawMeansQueue.size() >= 5)
                            rawMeansQueue.notify();
                    }

                    synchronized (queue) {
                        if (queue.size() >= 10)
                            queue.notify();
                    }
                }

                responseObserver.onNext(GlobalMeans.newBuilder()
                        .addAllGlobalMeans(kernel.getLatestGlobalMeasurements().values()).build());
            }

            @Override
            public void onError(Throwable t){
                LOGGER.warning("[meanStreamService] -> " + t.getMessage());
            }

            @Override
            public void onCompleted(){
                LOGGER.info("[meanStreamService] -> completed");

            }
        };
    }

    // utilizzato solo dal coordinatore
    @Override
    public StreamObserver<MeanMessage> rawMeansStream(final StreamObserver<MeanMessage> responseObserver){
        LOGGER.info("[rawMeansStreamService] -> new incoming connection");
        return new StreamObserver<MeanMessage>() {

            @Override
            public void onNext(MeanMessage messageIn){

                UUID uuid = UUID.fromString(messageIn.getId());
                String type = messageIn.getType();
                double value = messageIn.getValue();
                long timestamp = messageIn.getTimestamp();

                Measurement newMeasurement = new Measurement(uuid, type, value, timestamp);

                Set<Measurement> rawMeansQueue = kernel.getRawMeansSet();

                synchronized(rawMeansQueue){
                    rawMeansQueue.add(newMeasurement);
                    LOGGER.info("[rawMeansStreamService] -> mean [" + rawMeansQueue.size() + "] received from " + uuid);
                }

                responseObserver.onNext(MeanMessage.newBuilder().setTimestamp(timestamp).build());
            }

            @Override
            public void onError(Throwable t){
                LOGGER.warning("[rawMeanStreamService] -> " + t.getMessage());
            }

            @Override
            public void onCompleted() {
                LOGGER.info("[rawMeanStreamService] -> completed");
            }
        };
    }

    // private methods
    private void registerNewThread(Runnable routine) {
        kernel.registerThread(new Thread(routine)).start();
    }
}
