package services;

import communication.StreamHandler;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import it.unimi.sdp.SensorsServiceOuterClass.Ack;
import it.unimi.sdp.SensorsServiceOuterClass.LastMeasurement;
import it.unimi.sdp.SensorsServiceOuterClass.MeasurementMessage;
import it.unimi.sdp.SensorsServiceOuterClass.SensorPing;
import model.Kernel;
import model.Measurement;
import model.Network;
import model.Node;
import routines.MeasurementsRoutine;
import utils.CheckPeer;

import java.awt.*;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static it.unimi.sdp.Commons.NodeInfo;
import static it.unimi.sdp.Commons.SensorInfo;
import static it.unimi.sdp.FederationServiceGrpc.FederationServiceBlockingStub;
import static it.unimi.sdp.FederationServiceGrpc.newBlockingStub;
import static it.unimi.sdp.SensorsServiceGrpc.SensorsServiceImplBase;

public class SensorsServiceImpl extends SensorsServiceImplBase{
    private static final Logger LOGGER = Logger.getLogger("node");
    private Kernel kernel;
    private StreamHandler streamHandler;
    private Network network;

    public SensorsServiceImpl(Kernel kernel, StreamHandler streamHandler){
        this.kernel = kernel;
        this.streamHandler = streamHandler;
        this.network = kernel.getNetwork();
    }

    @Override
    public void check(SensorPing message, StreamObserver<Ack> responseObserver) {
        Ack ack = Ack.newBuilder().setStamp(network.getLatestEdit()).build();
        responseObserver.onNext(ack);
        responseObserver.onCompleted();
    }

    @Override
    public void nearestNode(SensorInfo message, StreamObserver<NodeInfo> responseObserver) {
        try {

            NodeInfo response;

            if (network.amIRoot()) {

                Point position = new Point(message.getPosition().getX(), message.getPosition().getY());
                Node nearestNode = network.getNearestNode(position);

                while (!CheckPeer.isPeerAlive(nearestNode)){
                    network.removeNode(nearestNode.getUuid());
                    nearestNode = network.getNearestNode(position);
                }

                response = NodeInfo.newBuilder()
                                .setUuid(nearestNode.getUuid().toString())
                                .setAddress(nearestNode.getAddress().toString())
                                .setFederationPort(nearestNode.getFederationPort())
                                .setAnalyticsPort(nearestNode.getAnalyticsPort())
                                .setPosition(NodeInfo.Position.newBuilder()
                                        .setX(nearestNode.getPosition().x)
                                        .setY(nearestNode.getPosition().y)
                                ).build();
            }

            else {
                final ManagedChannel channel = ManagedChannelBuilder.forAddress(
                        network.getRoot().getAddress().toString(),
                        network.getRoot().getFederationPort())
                        .usePlaintext().build();
                FederationServiceBlockingStub stub = newBlockingStub(channel);

                response = stub.nearestNode(message);

                channel.shutdown().awaitTermination(3, TimeUnit.SECONDS);
            }

                responseObserver.onNext(response);
                responseObserver.onCompleted();

        } catch (InterruptedException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    @Override
    public StreamObserver<MeasurementMessage> measurementsStream(final StreamObserver<LastMeasurement> responseObserver) {
        return new StreamObserver<MeasurementMessage>() {

            @Override
            public void onNext(MeasurementMessage messageIn){

                UUID uuid = kernel.getMe().getUuid();
                String type = messageIn.getType();
                double value = messageIn.getValue();
                long timestamp = messageIn.getTimestamp();

                Measurement newMeasurement = new Measurement(uuid, type, value, timestamp);

                Map<String, Queue<Measurement>> measurementsMap = kernel.getMeasurementsMap();

                synchronized (measurementsMap) {
                    if (measurementsMap.putIfAbsent(type, new ArrayDeque<>()) == null)
                        registerNewThread(new MeasurementsRoutine(type, kernel, streamHandler));
                }

                Queue<Measurement> queue = measurementsMap.get(type);

                synchronized (queue) {
                    queue.add(newMeasurement);
                    LOGGER.info("[" + messageIn.getId() + "] -> received measurement " + queue.size());
                }

                if ( ! network.amIRoot()) {
                    // if type is a new type, create a new handler thread for that type
                    synchronized (queue) {
                        if (queue.size() >= 40)
                            queue.notify();
                    }
                }

                responseObserver.onNext(LastMeasurement.newBuilder().setTimestamp(timestamp).build());

            }

            @Override
            public void onError(Throwable t){
                LOGGER.warning("[sensor] -> " + t.getMessage());
            }

            @Override
            public void onCompleted() {
                LOGGER.info("[sensor] -> completed");
            }

        };
    }

    // private methods
    private void registerNewThread(Runnable routine) {
        kernel.registerThread(new Thread(routine)).start();
    }
}