package controller;

import communication.RestClient;
import communication.StreamHandler;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import model.Kernel;
import model.Node;
import routines.BootRoutine;
import services.FederationServiceImpl;
import services.SensorsServiceImpl;

import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class NodeThread extends Thread{
    private final Node me;
    private final Kernel kernel;
    private final StreamHandler streamHandler;
    private static final Logger LOGGER = Logger.getLogger("node");

    public NodeThread(Kernel kernel) {
        this.kernel = kernel;
        this.me = kernel.getMe();
        this.streamHandler = new StreamHandler(kernel);
    }


    @Override
    public void run() {

        try {

            LOGGER.info("[BOOT] -> starting federation server @ " + me.getFederationPort());
            Server federationServer = ServerBuilder.forPort(me.getFederationPort())
                    .addService(new FederationServiceImpl(kernel, streamHandler)).build();
            federationServer.start();
            LOGGER.info("[BOOT] -> federation server started");

            LOGGER.info("[BOOT] -> starting analytics server @ " + me.getAnalyticsPort());
            Server analyticsServer = ServerBuilder.forPort(me.getAnalyticsPort())
                    .addService(new SensorsServiceImpl(kernel, streamHandler)).build();
            analyticsServer.start();
            LOGGER.info("[BOOT] -> analytics server started");

            LOGGER.info("[BOOT] -> starting bootRoutine");
            Thread bootThread = new Thread(new BootRoutine(kernel, streamHandler));
            bootThread.start();
            bootThread.join();
            LOGGER.info("[BOOT] -> bootRoutine started");

            ParentFailureThread parentFailureThread = new ParentFailureThread(kernel, streamHandler);
            parentFailureThread.start();

            loop();

            LOGGER.warning("[SHUTDOWN] -> starting shutdown routine");
            LOGGER.info("[SHUTDOWN] -> quitting federation server..");
            federationServer.shutdown();
            LOGGER.info("[SHUTDOWN] -> quitting analytics server..");
            analyticsServer.shutdown();

            federationServer.awaitTermination(2, TimeUnit.SECONDS);
            analyticsServer.awaitTermination(2, TimeUnit.SECONDS);

            LOGGER.info("[SHUTDOWN] -> stopping streams..");
            streamHandler.stopStream();
            LOGGER.info("[SHUTDOWN] -> stopping all threads..");
            kernel.stopAllThreads();
            LOGGER.info("[SHUTDOWN] -> quitting main thread..");


        } catch (IOException | InterruptedException e){
            LOGGER.severe(e.getMessage());
        }
    }

    private void loop() {
        while (true) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                String in = br.readLine();
                if (in.equals("q")) {
                    Response deletion = (new RestClient(kernel.getServerAddress())).removeNode(me);
                    switch (deletion.getStatus()) {
                        case 200:
                            return;
                        default:
                            System.out.println(deletion.getStatusInfo());
                    }
                }
            } catch (IOException e) {
                LOGGER.severe(e.getMessage());
            }
        }
    }
}
