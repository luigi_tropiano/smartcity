package controller;

import communication.StreamHandler;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import it.unimi.sdp.Commons;
import model.Kernel;
import model.Network;
import model.Node;
import sync.Syncelection;
import sync.Syncstate;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static it.unimi.sdp.FederationServiceGrpc.*;
import static it.unimi.sdp.FederationServiceOuterClass.*;

public class LocalElectionThread extends Thread {
    private static final Logger LOGGER = Logger.getLogger("node");
    private static final long TIMEOUT = 10000;
    private boolean answerReceived;
    private final Node me;
    private final Kernel kernel;
    private final Network network;
    private final StreamHandler streamHandler;
    private final Syncelection globalElection;
    private final Syncelection localElection;
    private final Syncstate localState;
    private final Set<ManagedChannel> electionSet;

    public LocalElectionThread(Kernel kernel, StreamHandler streamHandler) {
        this.answerReceived = false;
        this.kernel = kernel;
        this.streamHandler = streamHandler;
        this.me = kernel.getMe();
        this.network = kernel.getNetwork();
        this.globalElection = kernel.getSyncGlobalElection();
        this.localElection = kernel.getSyncLocalElection();
        this.localState = kernel.getLocalState();
        this.electionSet = new HashSet<>();
    }

    @Override
    public void run() {
        try {

            if (getPermissionToRun()){

                synchronized (localState) {
                    localState.setCriticalState();
                }

                synchronized (localState) {
                    while (localState.isCriticalState()) {
                        LOGGER.info("[localElection] -> running");
                        LOGGER.info("[localElection] -> asking for companions to coordinator");
                        Set<UUID> subnet = askForCompanions();
                        startElection(subnet);
                        LOGGER.info("[localElection] -> waiting for answers..");
                        waitForAnswers();

                        if (!answerReceived) {
                            LOGGER.info("[localElection] -> no answer received");
                            LOGGER.info("[localElection] -> I've won");
                            streamHandler.stopStream();
                            LOGGER.info("[localElection] -> announcing victory");
                            announceVictory(subnet);
                            LOGGER.info("[localElection] -> notifying coordinator");
                            notifyCoordinator();

                            network.setMyParent(network.getRoot().getUuid());
                            LOGGER.info("[localElection] -> I am the new local Parent");

                            localState.unsetCriticalState();
                            streamHandler.newStream();
                        } else
                            localState.wait(TIMEOUT);
                    }
                }

                synchronized (localElection) {
                    LOGGER.info("[localElection] -> quitting");
                    localElection.unsetRunning();
                    localElection.notifyAll();
                }
            }

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.severe(e.getMessage());
        }
    }

    private boolean getPermissionToRun(){
        synchronized (globalElection) {
            if (!globalElection.isRunning()) {
                synchronized (localElection) {
                    if (!localElection.isRunning()) {
                        localElection.setRunning();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private Set<UUID> askForCompanions() {
        Set<UUID> subnet = new HashSet<>();

        try {

            final ManagedChannel rootChannel = ManagedChannelBuilder.forAddress(
                    network.getRoot().getAddress().toString(),
                    network.getRoot().getFederationPort()).usePlaintext().build();

            ElectionMessage mex = ElectionMessage.newBuilder()
                    .setUuid(network.getMyParent().getUuid().toString())
                    .build();

            newBlockingStub(rootChannel)
                    .getCompanions(mex)
                    .getIdList()
                    .forEach(uuid -> subnet.add(UUID.fromString(uuid)));
        }

        catch(StatusRuntimeException ex){
            LOGGER.warning(ex.getMessage());
        }

        return subnet;
    }

    private void startElection(Set<UUID> subnet){
        if (subnet.isEmpty()) return;

        ElectionMessage message = ElectionMessage.newBuilder()
                .setUuid(me.getUuid().toString())
                .setParent(network.getMyParent().getUuid().toString())
                .build();

        subnet
                .parallelStream()
                .filter(uuid -> !uuid.equals(me.getUuid()))
                .filter(uuid -> uuid.compareTo(me.getUuid()) > 0)
                .forEach(uuid -> {

                    Node peer = network.getNode(uuid);

                    final ManagedChannel channel = ManagedChannelBuilder.forAddress(
                            peer.getAddress().toString(),
                            peer.getFederationPort()).usePlaintext().build();
                    //creating an asynchronous stub on the channel
                    FederationServiceStub stub = newStub(channel);

                    electionSet.add(channel);

                    stub.localElection(message, new StreamObserver<ElectionResponse>() {

                        @Override
                        public void onNext(ElectionResponse electionResponse) {
                            answerReceived = true;
                            synchronized (electionSet) {
                                electionSet.remove(channel);
                                electionSet.notify();
                            }
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            LOGGER.warning("[localElection] -> " + uuid + " :: " + throwable.getMessage());
                            closeChannel(channel);
                            LOGGER.warning("[localElection] -> " + uuid + " :: removing");
                            network.removeNode(peer.getUuid());

                            synchronized (electionSet) {
                                electionSet.remove(channel);
                                electionSet.notify();
                            }
                        }

                        @Override
                        public void onCompleted() {
                            closeChannel(channel);
                            LOGGER.info("[localElection] -> " + uuid + " :: completed");
                        }
                    });
                });
    }

    private void waitForAnswers() throws InterruptedException {
        while (!electionSet.isEmpty()) {
            synchronized (electionSet){
                electionSet.wait(TIMEOUT);
                LOGGER.info("[localElection] -> answers: " + electionSet.size());
            }
            electionSet.clear();
        }
    }

    private void announceVictory(Set<UUID> subnet) {

        ElectionMessage victory = ElectionMessage.newBuilder()
                .setUuid(me.getUuid().toString())
                .setParent(network.getMyParent().getUuid().toString())
                .build();

        subnet
                .stream()
                .filter(uuid -> !uuid.equals(me.getUuid()))
                .forEach((uuid -> {

                    Node peer = network.getNode(uuid);

                    final ManagedChannel channel = ManagedChannelBuilder
                            .forAddress(peer.getAddress().toString(), peer.getFederationPort())
                            .usePlaintext().build();

                    FederationServiceStub stub = newStub(channel);

                    stub.localVictory(victory, new StreamObserver<VictoryResponse>() {

                        @Override
                        public void onNext(VictoryResponse coordinatorResponse) {
                            if (coordinatorResponse.getConflict())
                                LOGGER.severe("[localElection] -> " + uuid + " :: conflict");
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            throwable.getCause().printStackTrace();
                            synchronized (network) {
                                closeChannel(channel);
                                LOGGER.warning("[localElection] -> " + uuid + " :: " + throwable.getMessage());
                            }
                        }

                        @Override
                        public void onCompleted() {
                            closeChannel(channel);
                            LOGGER.info("[localElection] -> " + uuid + " :: completed");
                        }

                    });
                }));
    }

    private void notifyCoordinator() {

        final ManagedChannel channel = ManagedChannelBuilder
                .forAddress(network.getRoot().getAddress().toString(), network.getRoot().getFederationPort())
                .usePlaintext().build();

        FederationServiceStub stub = newStub(channel);

        Commons.NodeInfo message = Commons.NodeInfo.newBuilder()
                .setUuid(me.getUuid().toString())
                .setAddress(me.getAddress().toString())
                .setFederationPort(me.getFederationPort())
                .setAnalyticsPort(me.getAnalyticsPort())
                .build();

        stub.newLocalParent(message, new StreamObserver<VictoryResponse>() {
            @Override
            public void onNext(VictoryResponse victoryResponse) {
                if (victoryResponse.getConflict())
                    LOGGER.severe("[localElection] -> coordinator :: conflict");
            }

            @Override
            public void onError(Throwable throwable) {
                closeChannel(channel);
                LOGGER.severe("[localElection] -> coordinator :: not notified");
            }

            @Override
            public void onCompleted() {
                closeChannel(channel);
                LOGGER.info("[localElection] -> coordinator :: notified");
            }
        });

    }

    private void closeChannel(ManagedChannel channel) {
        try {
            channel.shutdown();
            channel.awaitTermination(2, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.severe(e.getMessage());
        }
    }
}
