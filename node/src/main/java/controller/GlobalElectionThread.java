package controller;


import communication.StreamHandler;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import model.Kernel;
import model.Network;
import model.Node;
import routines.CheckPeerRoutine;
import sync.Syncelection;
import sync.Syncstate;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static it.unimi.sdp.FederationServiceGrpc.FederationServiceStub;
import static it.unimi.sdp.FederationServiceGrpc.newStub;
import static it.unimi.sdp.FederationServiceOuterClass.*;
import static java.util.Map.Entry;

public class GlobalElectionThread extends Thread {
    private static final Logger LOGGER = Logger.getLogger("node");
    private static final long TIMEOUT = 10000;
    private boolean answerReceived;
    private final Node me;
    private final Kernel kernel;
    private final Network network;
    private final StreamHandler streamHandler;
    private final Syncelection globalElection;
    private final Syncelection localElection;
    private final Syncstate globalState;
    private final Set<ManagedChannel> electionSet;

    public GlobalElectionThread(Kernel kernel, StreamHandler streamHandler) {
        this.answerReceived = false;
        this.kernel = kernel;
        this.streamHandler = streamHandler;
        this.me = kernel.getMe();
        this.network = kernel.getNetwork();
        this.globalElection = kernel.getSyncGlobalElection();
        this.localElection = kernel.getSyncLocalElection();
        this.globalState = kernel.getGlobalState();
        this.electionSet = new HashSet<>();
    }

    @Override
    public void run() {
        try {

            if (getPermissionToRun()) {

                synchronized (globalState) {
                    globalState.setCriticalState();
                }

                synchronized (localElection){
                    if (localElection.isRunning()) {
                        LOGGER.info("[globalElection] -> waiting for localElection to end..");
                        localElection.wait();
                        LOGGER.info("[globalElection] -> localElection ended");
                    }
                }

                synchronized (globalState) {
                    while (globalState.isCriticalState()) {
                        LOGGER.info("[globalElection] -> running");
                        startElection();
                        LOGGER.info("[globalElection] -> waiting for answers..");
                        waitForAnswers();

                        if (!answerReceived) {
                            LOGGER.info("[globalElection] -> no answer received");
                            LOGGER.info("[globalElection] -> I've won");

                            streamHandler.stopStream();

                            LOGGER.info("[globalElection] -> checking peers");
                            checkAllPeers();
                            LOGGER.info("[globalElection] -> updating network");
                            updateNetwork();
                            LOGGER.info("[globalElection] -> announcing victory");
                            announceVictory();
                            LOGGER.info("[globalElection] -> I am the coordinator now");

                            globalState.unsetCriticalState();

                        } else {
                            globalState.wait(TIMEOUT);
                        }
                    }
                }

                synchronized (globalElection) {
                    globalElection.unsetRunning();
                    LOGGER.info("[globalElection] -> quitting");
                }
            }

            else
                LOGGER.info("[globalElection] -> already running, ignoring");


        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.severe(e.getMessage());
        }
    }

    private boolean getPermissionToRun(){
        synchronized (globalElection) {
            if (!globalElection.isRunning()) {
                globalElection.setRunning();
                return true;
            }
            return false;
        }
    }

    private void startElection(){

        Map<UUID, Node> subNetwork = network
                .getCopy()
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().compareTo(me.getUuid()) > 0)
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

        if (subNetwork.isEmpty()) return;

        ElectionMessage message = ElectionMessage.newBuilder()
                .setUuid(me.getUuid().toString())
                .build();

        subNetwork.forEach((uuid, node) -> {
            if (!node.equals(me)) {

                final ManagedChannel channel = ManagedChannelBuilder
                        .forAddress(node.getAddress().toString(), node.getFederationPort())
                        .usePlaintext().build();

                electionSet.add(channel);
                FederationServiceStub stub = newStub(channel);

                stub.globalElection(message, new StreamObserver<ElectionResponse>() {

                    @Override
                    public void onNext(ElectionResponse electionResponse) {
                        answerReceived = true;
                        synchronized (electionSet) {
                            electionSet.remove(channel);
                            electionSet.notify();
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        LOGGER.warning("[globalElection] -> " + uuid + " :: " + throwable.getMessage());
                        closeChannel(channel);
                        LOGGER.warning("[globalElection] -> " + uuid + " :: removing");
                        network.removeNode(uuid);

                        synchronized (electionSet) {
                            electionSet.remove(channel);
                            electionSet.notify();
                        }
                    }

                    @Override
                    public void onCompleted() {
                        closeChannel(channel);
                        LOGGER.info("[globalElection] -> " + uuid + ":: completed");
                    }
                });
            }
        });
    }

    private void waitForAnswers() throws InterruptedException {
        synchronized (electionSet){
            while (!electionSet.isEmpty()) {
                electionSet.wait(TIMEOUT);
                LOGGER.info("[globalElection] -> answers: " + electionSet.size());
            }
            electionSet.clear();
        }
    }

    private void checkAllPeers() throws InterruptedException {
        Set<Thread> threads = new HashSet<>();

        for (Map.Entry<UUID, Node> entry : network.getCopy().entrySet()) {
            Thread thread = new Thread(new CheckPeerRoutine(network, entry.getValue()));
            LOGGER.info("[thread " + thread.getId() + "] -> checking " + entry.getKey());
            threads.add(thread);
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
            LOGGER.info("[thread " + thread.getId() + "] -> joined");
        }
    }

    private void updateNetwork(){
        network.setRoot(me.getUuid());
        network.setMyParent(null);
        network.rebuildTree();
    }

    private void announceVictory() {
        network.getCopy().forEach(((uuid, peer) -> {
            if (!uuid.equals(me.getUuid())) {

                final ManagedChannel channel = ManagedChannelBuilder
                        .forAddress(peer.getAddress().toString(), peer.getFederationPort())
                        .usePlaintext().build();

                FederationServiceStub stub = newStub(channel);

                ElectionMessage victory = ElectionMessage.newBuilder()
                        .setUuid(me.getUuid().toString())
                        .setParent(network.getPeerParent(uuid).toString())
                        .build();

                stub.globalVictory(victory, new StreamObserver<VictoryResponse>() {

                    @Override
                    public void onNext(VictoryResponse coordinatorResponse) {
                        if (coordinatorResponse.getConflict())
                            LOGGER.severe("[globalElection] -> " + uuid + " :: conflict");
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        synchronized (network) {
                            closeChannel(channel);
                            LOGGER.warning("[globalElection] -> " + uuid + " :: " + throwable.getMessage());
                        }
                    }

                    @Override
                    public void onCompleted() {
                        closeChannel(channel);
                        LOGGER.info("[globalElection] -> " + uuid + " :: completed");
                    }
                });
            }
        }));
    }

    private void closeChannel(ManagedChannel channel){
        try {
            channel.shutdown().awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.severe(e.getMessage());
        }
    }
}
