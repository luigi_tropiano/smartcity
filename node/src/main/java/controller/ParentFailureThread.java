package controller;

import communication.StreamHandler;
import model.Kernel;
import model.Network;
import model.Node;
import sync.Syncstate;
import utils.CheckPeer;

import java.util.logging.Logger;

public class ParentFailureThread extends Thread{
    private static final Logger LOGGER = Logger.getLogger("node");

    private final Kernel kernel;
    private final StreamHandler streamHandler;

    public ParentFailureThread(Kernel kernel, StreamHandler streamHandler) {
        this.kernel = kernel;
        this.streamHandler = streamHandler;

        kernel.registerThread(this);
    }

    @Override
    public void run() {
        try {

            while (!this.isInterrupted()) {

                synchronized (kernel) {
                    kernel.wait();
                }

                LOGGER.info("[parentFailureThread] -> awakened");

                Network network = kernel.getNetwork();
                Node parent = network.getMyParent();

                if (!CheckPeer.isPeerAlive(parent)) {

                    LOGGER.info("[" + parent.getUuid() + "] -> not responding");
                    removePeer(network, parent);

                    if (parent.equals(network.getRoot())) {
                            LOGGER.info("[globalElection] -> starting..");
                            GlobalElectionThread globalElectionThread = new GlobalElectionThread(kernel, streamHandler);
                            globalElectionThread.start();
                            globalElectionThread.join();
                            LOGGER.info("[globalElection] -> terminated");
                    } else if (parent.equals(network.getMyParent())) {
                            LOGGER.info("[localElection] -> starting..");
                            LocalElectionThread localElectionThread = new LocalElectionThread(kernel, streamHandler);
                            localElectionThread.start();
                            localElectionThread.join();
                            LOGGER.info("[localElection] -> terminated");

                    } else
                        LOGGER.severe("[" + parent.getUuid() + "] is neither coordinator nor my father");
                }
                else {
                    streamHandler.newStream();
                }
            }

        } catch (InterruptedException e) {
            LOGGER.severe("[parentFailureThread] -> interrupted");
        }
    }


    private void removePeer(Network network, Node parent){
        synchronized (network) {
            network.removeNode(parent.getUuid());
        }
        LOGGER.info("[" + parent.getUuid() + "] -> removed");
    }
}
