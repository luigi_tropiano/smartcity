package utils;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.util.UUID;

public class UuidValidator implements IParameterValidator {
    @Override
    public void validate(String name, String value) throws ParameterException {
        try {
            UUID.fromString(value);
        } catch (IllegalArgumentException e){
            throw new ParameterException("Parameter " + name + " should be a valid UUID");
        }
    }
}
