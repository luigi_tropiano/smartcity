package utils;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.util.Arrays;
import java.util.List;

public class LogLevelValidator implements IParameterValidator {
    private final static List<String> validLevels = Arrays.asList(
            "all",
            "info",
            "warning",
            "severe",
            "off"
    );

    @Override
    public void validate(String name, String value) throws ParameterException {
        if ( ! validLevels.contains(value)) {
            throw new ParameterException("Parameter " + name + " should be one of " + validLevels);
        }
    }

}
