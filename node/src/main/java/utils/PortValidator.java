package utils;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class PortValidator  implements IParameterValidator {
    @Override
    public void validate(String name, String value) throws ParameterException {
        int n = Integer.parseInt(value);
        if (n < 1024) {
            throw new ParameterException("Parameter " + name + " should be equal or greater than 1024 (found " + value + ")");
        }
    }
}
