package utils;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestLine;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import it.unimi.sdp.FederationServiceOuterClass.MeanMessage;
import model.Measurement;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public final class TablePrinter {

    private TablePrinter(){}

    public static void printLocal(Map<String, List<Measurement>> map){

        map.forEach((type, list) -> {
            AsciiTable at = new AsciiTable();
            at.addRule();
            at.addRow(null, type);
            at.addRule();
            at.addRow("Timestamp", "Value");
            at.addRule();
            list.stream().sorted(Comparator.comparing(Measurement::getTimestamp).reversed()).limit(8).forEach(measurement -> {
                at.addRow(new Timestamp(measurement.getTimestamp()), measurement.getValue());
                at.addRule();
            });
            renderTable(at);
        });
    }

    public static void printGlobal(Map<String, MeanMessage> measurements){
        if (!measurements.isEmpty()) {
            AsciiTable at = new AsciiTable();

            at.addRule();
            at.addRow(null, null, "GLOBAL MEANS");
            at.addRule();
            at.addRow("Type", "Timestamp", "Value");
            at.addRule();
            measurements.forEach((type, mean) -> {
                at.addRow(type, new Timestamp(mean.getTimestamp()), mean.getValue());
                at.addRule();
            });
            renderTable(at);
        }
    }

    private static void renderTable(AsciiTable table){
        CWC_LongestLine cwc = new CWC_LongestLine();
        table.setPaddingLeft(2);
        table.setPaddingRight(2);
        table.setTextAlignment(TextAlignment.CENTER);
        table.getRenderer().setCWC(cwc);
        System.out.println(table.render());

    }

}
