package utils;

import com.beust.jcommander.Parameter;

import java.net.URI;
import java.util.UUID;
import java.util.logging.Level;

public class Args {

    @Parameter(names = {"--uuid", "-u"},
            description = "UUID of the node",
            order = 0,
            validateWith = UuidValidator.class,
            converter = UuidConverter.class)
    private UUID uuid;

    @Parameter(names = {"--federation", "-f"},
            description = "Port for node-to-node communication",
            //required = true,
            order = 1,
            validateWith = PortValidator.class)
    private Integer federation;

    @Parameter(names = {"--analytics", "-a"},
            description = "Port for measurements communication",
            //required = true,
            order = 2,
            validateWith = PortValidator.class)
    private Integer analytics;

    @Parameter(names = {"--serverAddress", "-s"},
            description = "Address of the remote server",
            order = 3,
            converter = UriConverter.class,
            validateWith = UriValidator.class)
    private URI serverAddress;

    @Parameter(names = {"--log", "-l"},
            description = "Log level ( all | info | warning | severe | off )",
            order = 4,
            converter = LogLevelConverter.class,
            validateWith = LogLevelValidator.class)
    private Level logLevel;

    @Parameter(names = {"--help", "-h"},
            description = "Show this message",
            order = 5,
            help = true)
    private boolean help = false;


    public Args (){}

    public UUID getUuid() {
        return uuid;
    }

    public Integer getFederation() {
        return federation;
    }

    public Integer getAnalytics() {
        return analytics;
    }

    public URI getServerAddress() {
        return serverAddress;
    }

    public Level getLogLevel() {
        return logLevel;
    }

    public boolean isHelp() {
        return help;
    }

}
