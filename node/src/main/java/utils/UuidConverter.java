package utils;

import com.beust.jcommander.IStringConverter;

import java.util.UUID;

public class UuidConverter implements IStringConverter<UUID> {
    @Override
    public UUID convert(String s) {
        return UUID.fromString(s);
    }
}
