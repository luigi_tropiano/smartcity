package utils;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import it.unimi.sdp.FederationServiceGrpc;
import it.unimi.sdp.FederationServiceOuterClass;
import model.Node;

import static it.unimi.sdp.FederationServiceGrpc.newBlockingStub;
import static it.unimi.sdp.FederationServiceOuterClass.Ping.newBuilder;

public class CheckPeer {

    private CheckPeer(){}

    public static boolean isPeerAlive(Node node) {

        int i = 3;

        while (true) {

            if (i == 0)
                return false;
            if (!pingPeer(node))
                i--;
            else
                return true;

            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean pingPeer(Node node){

        String address = node.getAddress().toString();
        int port = node.getFederationPort();
        final ManagedChannel channel = ManagedChannelBuilder.forAddress(address, port).usePlaintext().build();
        FederationServiceGrpc.FederationServiceBlockingStub stub = newBlockingStub(channel);
        FederationServiceOuterClass.Ping ping = newBuilder().build();

        try {
            stub.isPeerAlive(ping);
            channel.shutdown();
            return true;
        }
        catch (StatusRuntimeException ex) {
            System.out.println(ex.getMessage());
            //logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            channel.shutdown();
            return false;
        }
    }
}
