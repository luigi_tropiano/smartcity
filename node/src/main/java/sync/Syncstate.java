package sync;

public class Syncstate {

    private boolean criticalState;

    public Syncstate(){
        criticalState = false;
    }

    public boolean isCriticalState() {
        return criticalState;
    }

    public void setCriticalState(){
        criticalState = true;
    }

    public void unsetCriticalState(){
        criticalState = false;
    }
}
