package sync;

public class Syncelection {
    private boolean electionRunning;

    public Syncelection(){
        electionRunning = false;
    }

    public boolean isRunning() {
        return electionRunning;
    }

    public void setRunning(){
        electionRunning = true;
    }

    public void unsetRunning(){
        electionRunning = false;
    }
}
