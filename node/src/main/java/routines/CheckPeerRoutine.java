package routines;

import model.Network;
import model.Node;
import utils.CheckPeer;

public class CheckPeerRoutine implements Runnable {
    private final Network network;
    private final Node node;


    public CheckPeerRoutine(Network network, Node node){
        this.network = network;
        this.node = node;
    }

    @Override
    public void run() {
        if (!CheckPeer.isPeerAlive(node)) {
            System.out.println("REMOVING PEER " + node.getUuid());
            network.removeNode(node.getUuid());
        }
    }
}
