package routines;

import communication.RestClient;
import model.Kernel;
import model.Measurement;
import utils.StatUtil;
import utils.TablePrinter;

import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Logger;

import static it.unimi.sdp.FederationServiceOuterClass.MeanMessage;
import static java.util.Objects.requireNonNull;

public class RootRoutine implements Runnable{
    private static final Logger LOGGER = Logger.getLogger("node");
    private final Kernel kernel;
    private final Map<String, Measurement> globalMeansMap;

    public RootRoutine(Kernel kernel){
        this.kernel = kernel;
        this.globalMeansMap = new HashMap<>();
    }

    @Override
    public void run() {
        try {

            while (!Thread.currentThread().isInterrupted()) {

                Thread.sleep(5000);

                computeGlobalMeans();

                forwardRawMeansToServer();

                sendGlobalMeansToServer();

            }

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.severe("[rootRoutine] -> interrupted");
        }
    }

    private void computeGlobalMeans(){


        synchronized (kernel.getMeansMap()){

            kernel.getMeansMap().forEach((type, queue) -> {

                List<Double> list = new ArrayList<>();

                synchronized (queue) {
                    if ( ! queue.isEmpty()) {
                        queue.forEach(measurement -> list.add(requireNonNull(queue.poll()).getValue()));
                    }
                }

                if ( ! list.isEmpty()) {

                    Double mean = StatUtil.getMean(list);
                    Measurement measurement = new Measurement(
                            kernel.getMe().getUuid(),
                            type,
                            mean,
                            System.currentTimeMillis()
                    );

                    LOGGER.info("[" + type + "] -> computed :: " +
                            measurement.getValue() + " @ " + new Timestamp(measurement.getTimestamp()));

                    globalMeansMap.put(type, measurement);

                    synchronized (kernel.getLatestGlobalMeasurements()){
                        kernel.getLatestGlobalMeasurements().put(
                                measurement.getType(),
                                MeanMessage.newBuilder()
                                        .setId(measurement.getUuid().toString())
                                        .setType(measurement.getType())
                                        .setValue(measurement.getValue())
                                        .setTimestamp(measurement.getTimestamp()).build());
                        TablePrinter.printGlobal(kernel.getLatestGlobalMeasurements());
                    }
                }
            });
        }
    }

    private void forwardRawMeansToServer(){
        RestClient restClient = new RestClient(kernel.getServerAddress());

        Set<Measurement> copy;
        synchronized (kernel.getRawMeansSet()){
            copy = new HashSet<>(kernel.getRawMeansSet());
            kernel.getRawMeansSet().clear();
        }

        Response response;
        if ( ! copy.isEmpty()){
            LOGGER.info("[rootRoutine] -> forwarding raw means to server");
            response = restClient.sendMeasurements(copy);
            LOGGER.info("[server] -> " + response.getStatusInfo());
        }
    }

    private void sendGlobalMeansToServer(){
        RestClient restClient = new RestClient(kernel.getServerAddress());

        Map<String, Measurement> copy;
        synchronized (globalMeansMap){
            copy = new HashMap<>(globalMeansMap);
            globalMeansMap.clear();
        }

        Response response;
        if ( ! copy.isEmpty()) {
            LOGGER.info("[rootRoutine] -> sending global means map to server");
            response = restClient.sendGlobalMeasurements(copy);
            LOGGER.info("[server] -> " + response.getStatusInfo());
        }
    }
}
