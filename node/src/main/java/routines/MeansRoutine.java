package routines;

import communication.StreamHandler;
import model.Kernel;
import model.Measurement;
import utils.StatUtil;
import utils.TablePrinter;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

import static java.util.Objects.requireNonNull;

public class MeansRoutine implements Runnable{
    private static final Logger LOGGER = Logger.getLogger("node");
    private static final int COUNT = 10;
    private final String type;
    private final Kernel kernel;
    private final StreamHandler streamHandler;
    private final Queue<Measurement> queue;
    private final List<Double> partialMeans;

    public MeansRoutine(String type, Kernel kernel, StreamHandler streamHandler){
        this.type = type;
        this.kernel = kernel;
        this.streamHandler = streamHandler;
        this.queue = kernel.getMeansMap().get(type);
        this.partialMeans = new ArrayList<>();
    }

    @Override
    public void run() {
        try {

            while (!Thread.currentThread().isInterrupted()) {

                synchronized (queue) {
                    queue.wait();
                }

                List<Double> slidingWindow = new ArrayList<>();

                synchronized (queue) {
                    if (queue.size() >= COUNT) {
                        for (int i = 0; i < COUNT /2; i++) {
                            slidingWindow.add(requireNonNull(queue.poll()).getValue());
                            //LOGGER.info("polling queue [" + queue.size() + "]");
                        }

                        partialMeans.add(StatUtil.getMean(slidingWindow));

                        if (partialMeans.size() == 2) {
                            double finalMean = StatUtil.getMean(partialMeans);
                            partialMeans.remove(0);

                            Measurement measurement = new Measurement(
                                    kernel.getMe().getUuid(),
                                    type,
                                    finalMean,
                                    System.currentTimeMillis());

                            synchronized (kernel.getLocalHistoryMap()) {
                                kernel.getLocalHistoryMap().putIfAbsent(measurement.getType(), new ArrayList<>());
                                kernel.getLocalHistoryMap().get(measurement.getType()).add(measurement);
                                TablePrinter.printLocal(kernel.getLocalHistoryMap());
                                TablePrinter.printGlobal(kernel.getLatestGlobalMeasurements());
                            }

                            sendMeanToParent(measurement);
                        }
                    }
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.severe("[meansRoutine] -> interrupted");
        }
    }

    private void sendMeanToParent(Measurement measurement){
        streamHandler.sendMean(measurement);
    }
}
