package routines;

import communication.StreamHandler;
import model.Measurement;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Logger;


public class RawMeansRoutine implements Runnable{
    private static final Logger LOGGER = Logger.getLogger("node");
    private final StreamHandler streamHandler;
    private final Set<Measurement> rawMeansQueue;

    public RawMeansRoutine(Set<Measurement> rawMeansQueue, StreamHandler streamHandler){
        this.streamHandler = streamHandler;
        this.rawMeansQueue = rawMeansQueue;
    }

    @Override
    public void run() {
        try {

            while (!Thread.currentThread().isInterrupted()) {
                synchronized (rawMeansQueue) {
                    rawMeansQueue.wait();
                }
                sendRawMeansToParent();
            }

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.severe("[rawMeansRoutine] -> interrupted");
        }
    }

    private void sendRawMeansToParent(){
        Queue<Measurement> copy;
        synchronized (rawMeansQueue) {
            copy = new ArrayDeque<>(rawMeansQueue);
            rawMeansQueue.clear();
        }
        copy.forEach(streamHandler::sendRawMeans);
    }

}
