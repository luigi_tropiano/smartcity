package routines;

import communication.StreamHandler;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import it.unimi.sdp.Commons.NodeInfo;
import model.Kernel;
import model.Network;
import model.Node;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static it.unimi.sdp.FederationServiceGrpc.FederationServiceStub;
import static it.unimi.sdp.FederationServiceGrpc.newStub;
import static it.unimi.sdp.FederationServiceOuterClass.HelloResponse;

public class BootRoutine implements Runnable {
    private static final Logger LOGGER = Logger.getLogger("node");
    private static final long TIMEOUT = 10000;
    private final Kernel kernel;
    private final Node me;
    private final Network network;
    private final StreamHandler streamHandler;
    private final Set<ManagedChannel> answersSet = new HashSet<>();

    public BootRoutine(Kernel kernel, StreamHandler streamHandler){
        this.kernel = kernel;
        this.streamHandler = streamHandler;
        this.me = kernel.getMe();
        this.network = kernel.getNetwork();
    }

    @Override
    public void run() {

        if (amIFirstNode()) {
            LOGGER.info("[bootRoutine] -> I am the coordinator");
            network.setRoot(me.getUuid());
            network.setMyParent(null);
            kernel.registerThread(new Thread(new RootRoutine(kernel))).start();
        }

        else {
            while (network.getRoot() == null) {
                try {
                    LOGGER.info("[bootRoutine] -> announcing to other peers");
                    introduceToOthers();
                    LOGGER.info("[bootRoutine] -> waiting for answers");
                    waitForAnswers();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    LOGGER.severe("[meansRoutine] -> interrupted");
                }
            }
            streamHandler.newStream();
        }
    }

    private boolean amIFirstNode(){
        if (network.size() == 0)
            return true;
        else return network.size() == 1 && network.getCopy().containsKey(me.getUuid());
    }

    private void introduceToOthers(){

        Map<UUID, Node> copy = network.getCopy();

        copy.forEach((uuid, node) -> {
            if (!uuid.equals(me.getUuid())) {

                NodeInfo message = NodeInfo.newBuilder()
                        .setUuid(me.getUuid().toString())
                        .setAddress(me.getAddress().toString())
                        .setFederationPort(me.getFederationPort())
                        .setAnalyticsPort(me.getAnalyticsPort())
                        .setPosition(NodeInfo.Position.newBuilder()
                                .setX(me.getPosition().x)
                                .setY(me.getPosition().y).build())
                        .build();

                final ManagedChannel channel = ManagedChannelBuilder
                        .forAddress(node.getAddress().toString(), node.getFederationPort()).usePlaintext().build();

                answersSet.add(channel);

                //creating an asynchronous stub on the channel
                FederationServiceStub stub = newStub(channel);

                stub.helloRequest(message, new StreamObserver<HelloResponse>() {

                    @Override
                    public void onNext(HelloResponse helloResponse) {

                        boolean isCoordinator = helloResponse.getIsCoordinator();
                        String parentUuid = helloResponse.getParent();

                        if (isCoordinator) {
                            if (!parentUuid.isEmpty()) {
                                UUID parent = UUID.fromString(parentUuid);
                                network.setRoot(node.getUuid());
                                network.setMyParent(parent);
                                LOGGER.warning("[bootRoutine] -> " + uuid + " is coordinator");
                                LOGGER.warning("[bootRoutine] -> " + parentUuid + " is my parent");
                            }
                        }

                        synchronized (answersSet) {
                            answersSet.remove(channel);
                            answersSet.notify();
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        LOGGER.warning("[bootRoutine] -> " + uuid + " :: " + throwable.getMessage());
                        closeChannel(channel);
                        LOGGER.warning("[bootRoutine] -> " + uuid + " :: removing");
                        network.removeNode(uuid);

                        synchronized (answersSet) {
                            answersSet.remove(channel);
                            answersSet.notify();
                        }
                    }

                    @Override
                    public void onCompleted() {
                        closeChannel(channel);
                        LOGGER.info("[bootRoutine] -> " + uuid + " :: handshake completed");

                    }
                });
            }

            });
    }

    private void waitForAnswers() throws InterruptedException {
        synchronized (answersSet){
            while (!answersSet.isEmpty()) {
                answersSet.wait(TIMEOUT);
                LOGGER.info("[bootRoutine] -> answers: " + answersSet.size());
            }
            answersSet.clear();
        }
    }

    private void closeChannel(ManagedChannel channel){
        try {
            channel.shutdown().awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.severe(e.getMessage());
        }
    }
}
