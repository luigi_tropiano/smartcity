package utils;

import java.util.List;

public class StatUtil {

    private StatUtil(){}

    public static double getVariance(List<Double> measurements){
        double sqrDiff = 0;
        double mean = getMean(measurements);
        for (Double value : measurements)
            sqrDiff += Math.pow((value - mean), 2);
        return (sqrDiff/(measurements.size() - 1));
    }

    public static double getMean(List<Double> measurements){
        double sum = 0;
        for (Double  value : measurements)
            sum += value;
        return sum/measurements.size();
    }
}
