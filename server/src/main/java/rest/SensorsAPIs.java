package rest;

import controller.SensorsController;
import model.City;
import model.CityHandler;
import model.Node;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.util.logging.Logger;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SensorsAPIs {
    private static final Logger LOGGER = Logger.getLogger("server");
    private final City city;
    private SensorsController sensorsController;

    public SensorsAPIs() {
        this.city = CityHandler.getInstance().getCity();
        this.sensorsController = new SensorsController(city);
    }

    @GET
    @Path("/nearbynode")
    public Response getNearbyNode(@QueryParam("x")Integer x, @QueryParam("y")Integer y){
        if (x == null || y == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        LOGGER.info("[GET] nearby for (x = " + x + ", y = " + y + ")");
        synchronized (city){
            Node nearestNode = sensorsController.getNearestNode(new Point(x, y));
            if (nearestNode == null)
                return Response.noContent().build();
            LOGGER.info("nearest node for (x = " + x + ", y = " + y + ") is " + nearestNode.getPosition());
            return Response.ok(nearestNode).build();
        }
    }
}