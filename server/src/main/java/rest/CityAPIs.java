package rest;

import controller.CityController;
import model.City;
import model.CityHandler;
import model.Measurement;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.logging.Logger;

@Path("/city")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CityAPIs {
    private static final Logger LOGGER = Logger.getLogger("server");
    private final City city;
    private final CityController cityController;

    public CityAPIs() {
        this.city = CityHandler.getInstance().getCity();
        this.cityController = new CityController(city);
    }

    @GET
    @Path("/state")
    public Response getCityState(){
        return Response.ok(cityController.getCityState()).build();
    }

    @GET
    @Path("/measurements")
    public Response getCityMeasurements(@QueryParam("type") String type, @QueryParam("n") Integer n){
        if (type == null)
            return Response.ok(cityController.getCityMeasurements(n)).build();
        return Response.ok(cityController.getCityMeasurements(type, n)).build();
    }

    @GET
    @Path("/means")
    public Response getCityMean(@QueryParam("type") String type, @QueryParam("n") Integer n){
        if (type == null)
            return Response.ok(cityController.getCityMeans(n)).build();
        return Response.ok(cityController.getCityMean(type, n)).build();
    }

    @GET
    @Path("/stdevs")
    public Response getCityStandardDeviation(@QueryParam("type") String type, @QueryParam("n") Integer n){
        if (type == null)
            return Response.ok(cityController.getCityStandardDeviations(n)).build();
        return Response.ok(cityController.getCityStandardDeviation(type, n)).build();
    }

    @POST
    @Path("/measurements")
    public Response postGlobalMeasurements(Map<String, Measurement> map){
        if (cityController.postGlobalMeasurements(map))
            return Response.ok().build();
        return Response.status(Response.Status.CONFLICT).build();
    }
}