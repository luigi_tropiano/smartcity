package rest;

import controller.NodesController;
import model.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

@Path("/nodes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class NodesAPIs {
    private static final Logger LOGGER = Logger.getLogger("server");
    private final City city;
    private final NodesController nodesController;

    public NodesAPIs() {
        this.city = CityHandler.getInstance().getCity();
        this.nodesController = new NodesController(city);
    }

    @POST
    @Path("/")
    public Response postNode(Node node){
        LOGGER.info("[POST] from " + node.getUuid() + " @ " + node.getPosition().toString());
        synchronized (CityHandler.getInstance().getCity()) {
            int result = nodesController.addNode(node);
            if (result == 3)
                return Response.status(Response.Status.CONFLICT).build();
            if (result == 2)
                return Response.status(Response.Status.CONFLICT).build();
            if (result == 1)
                return Response.status(Response.Status.CONFLICT).build();

            LOGGER.info("[" + node.getUuid() + "] added succesfully");
            Map<UUID, Node> network = nodesController.getNodesMap();
            return Response.ok(new WelcomeMessage(network)).build();
        }
    }

    @PUT
    @Path("/{uuid}")
    public Response putNode(@PathParam("uuid") String uuid, Node node) {
        LOGGER.info("[PUT] from " + node.getUuid() + " @ " + node.getPosition().toString());
        synchronized (CityHandler.getInstance().getCity()) {
            if (!node.getUuid().equals(UUID.fromString(uuid)))
                return Response.status(Response.Status.BAD_REQUEST).build();
            int result = nodesController.addNode(node);
            if (result == 3)
                return Response.status(Response.Status.CONFLICT).build();
            if (result == 2)
                return Response.status(Response.Status.CONFLICT).build();
            if (result == 1)
                return Response.status(Response.Status.CONFLICT).build();
            LOGGER.info("[" + node.getUuid() + "] added succesfully");

            Map<UUID, Node> network = nodesController.getNodesMap();
            return Response.ok(new WelcomeMessage(network)).build();
        }
    }

    @DELETE
    @Path("/{uuid}")
    public Response delete(@PathParam("uuid")String uuid) {
        LOGGER.info("[DELETE] from " + uuid);
        synchronized (city) {
            int result = nodesController.deleteNode(UUID.fromString(uuid));
            if (result == 3)
                return Response.status(Response.Status.NOT_FOUND).build();
            LOGGER.info("[" + uuid + "] removed succesfully");
            return Response.ok().build();
        }
    }

    // Analytics
    @GET
    @Path("/measurements")
    public Response getNodesMeasurements(@QueryParam("type") String type, @QueryParam("n") Integer n){
            if (type == null)
                return Response.ok(nodesController.getLocalMeasurements(n)).build();
            if (type.equals("all"))
                return Response.ok(nodesController.getAllTypesLocalMeasurements(n)).build();
            return Response.ok(nodesController.getLocalMeasurements(type, n)).build();
    }

    @GET
    @Path("/{uuid}/measurements")
    public Response getNodeMeasurements(
            @PathParam("uuid") String uuid, @QueryParam("type") String type, @QueryParam("n") Integer n){
            if (type == null)
                return Response.ok(nodesController.getNodeMeasurements(UUID.fromString(uuid), n)).build();
            if (type.equals("all"))
                return Response.ok(nodesController.getAllTypesNodeMeasurements(UUID.fromString(uuid), n)).build();
            return Response.ok(nodesController.getNodeMeasurements(UUID.fromString(uuid), type, n)).build();
    }

    @GET
    @Path("/{uuid}/means")
    public Response getNodeMean(
            @PathParam("uuid")String uuid, @QueryParam("type") String type, @QueryParam("n") Integer n){
            if (type == null)
                return Response.ok(nodesController.getNodeMeans(UUID.fromString(uuid), n)).build();
            return Response.ok(nodesController.getNodeMeans(UUID.fromString(uuid), type, n)).build();
    }
    @GET
    @Path("/{uuid}/stdevs")
    public Response getNodeStandardDeviation(
            @PathParam("uuid") String uuid, @QueryParam("type") String type, @QueryParam("n") Integer n){
        if (type == null)
            return Response.ok(nodesController.getNodeStandardDeviations(UUID.fromString(uuid), n)).build();
        return Response.ok(nodesController.getNodeStandardDeviations(UUID.fromString(uuid), type, n)).build();
    }

    @POST
    @Path("/measurements")
    public Response postNodeMeasurements(Set<Measurement> measurements){
        return Response.ok(nodesController.postMeasurements(measurements)).build();
    }
}