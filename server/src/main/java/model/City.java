package model;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class City {
    private Dimension dimension;
    private int minDistance;
    private Map <UUID, Node> nodesMap;
    //private TreeMap <Long, Measurement> measurementsMap;
    private Map<String, TreeMap<Long, Measurement>> measurementsMap;

    public City(Dimension dimension) {
        this.minDistance = 20;
        this.dimension = dimension;
        this.nodesMap = new HashMap<>();
        this.measurementsMap = new HashMap<>();
    }

    public Dimension getDimension() {
        return new Dimension(dimension);
    }

    public int getWidth(){
        return dimension.width;
    }

    public int getHeight(){
        return dimension.height;
    }

    public int getMinDistance(){
        return minDistance;
    }

    public Map<UUID, Node> getNodesMap() {
        return new HashMap<>(nodesMap);
    }

    public Node addNode(Node node) {
        return nodesMap.put(node.getUuid(), node);
    }

    public Node removeNode(UUID uuid){
        return nodesMap.remove(uuid);
    }

    public Map<String, TreeMap<Long, Measurement>> getMeasurementsMap() {
        return new HashMap<>(measurementsMap);
    }

    public TreeMap<Long, Measurement> getMeasurementsMap(String type){
        measurementsMap.putIfAbsent(type, new TreeMap<>());
        return new TreeMap<>(measurementsMap.get(type));
    }

    public Measurement addMeasurement(Measurement measurement) {
        measurementsMap.putIfAbsent(measurement.getType(), new TreeMap<>());
        return measurementsMap.get(measurement.getType()).put(measurement.getTimestamp(), measurement);
    }
}