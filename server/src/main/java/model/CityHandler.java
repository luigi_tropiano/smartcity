package model;

import java.awt.*;

public class CityHandler {
    private static CityHandler instance;
    private City city;

    private CityHandler(){
        city = new City(new Dimension(100, 100));
    }

    public static synchronized CityHandler getInstance() {
        if(instance == null) instance = new CityHandler();
        return instance;
    }

    public City getCity() {
        return city;
    }

}
