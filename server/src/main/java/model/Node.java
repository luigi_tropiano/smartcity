package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.awt.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class Node {
    @JsonProperty ("uuid") private UUID uuid;
    @JsonProperty ("position") private Point position;
    @JsonProperty ("address") private URI address;
    @JsonProperty ("federationPort") private int federationPort;
    @JsonProperty ("analyticsPort") private int analyticsPort;
    //@JsonIgnore private TreeMap <Long, Measurement> measurements;
    @JsonIgnore private Map<String, TreeMap<Long, Measurement>> measurements;

    public Node(Node node) {
        this.uuid = node.uuid;
        this.position = node.position;
        this.address = node.address;
        this.analyticsPort = node.analyticsPort;
        this.federationPort = node.federationPort;
        this.measurements = new HashMap<>();
    }

    @JsonCreator
    public Node(@JsonProperty("uuid") UUID uuid,
                @JsonProperty("position") Point position,
                @JsonProperty("address") URI address,
                @JsonProperty("analyticsPort") int analyticsPort,
                @JsonProperty("federationPort") int federationPort) {
        this.uuid = uuid;
        this.position = position;
        this.address = address;
        this.analyticsPort = analyticsPort;
        this.federationPort = federationPort;
        this.measurements = new HashMap<>();
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public Point getPosition() {
        return new Point(position);
    }

    public void setAddress(URI address) {
        this.address = address;
    }

    public URI getAddress() {
        return address;
    }

    public void setAnalyticsPort(int analyticsPort) {
        this.analyticsPort = analyticsPort;
    }

    public int getAnalyticsPort() {
        return analyticsPort;
    }

    public void setFederationPort(int federationPort) {
        this.federationPort = federationPort;
    }

    public int getFederationPort() {
        return federationPort;
    }

    public Map<String, TreeMap<Long, Measurement>> getMeasurements() {
        return new HashMap<>(measurements);
    }

    public TreeMap<Long, Measurement> getMeasurements(String type) {
        measurements.putIfAbsent(type, new TreeMap<>());
        return new TreeMap<>(measurements.get(type));
    }

    public Measurement addMeasurement(Measurement measurement) {
        measurements.putIfAbsent(measurement.getType(), new TreeMap<>());
        return measurements.get(measurement.getType()).put(measurement.getTimestamp(), measurement);
    }
}
