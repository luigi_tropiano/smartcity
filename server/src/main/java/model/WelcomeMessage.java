package model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.UUID;

public class WelcomeMessage {
    @JsonProperty("network") private Map<UUID, Node> network;

    public WelcomeMessage(Map<UUID, Node> network){
        this.network = network;
    }

    public Map<UUID, Node> getNetwork() {
        return network;
    }
}
