package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Measurement implements Comparable<Measurement> {
    private final UUID uuid;
    private final String type;
    private final double value;
    private final long timestamp;

    @JsonCreator
    public Measurement(
            @JsonProperty("uuid") UUID uuid,
            @JsonProperty("type") String type,
            @JsonProperty("value") double value,
            @JsonProperty("timestamp") long timestamp) {
        this.uuid=uuid;
        this.type=type;
        this.value = value;
        this.timestamp = timestamp;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getType() {
        return type;
    }

    public double getValue() {
        return value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public int compareTo(Measurement m) {
        Long thisTimestamp = timestamp;
        Long otherTimestamp = m.getTimestamp();
        return thisTimestamp.compareTo(otherTimestamp);
    }

    public String toString(){
        return value + " " + timestamp;
    }
}
