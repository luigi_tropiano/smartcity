package controller;

import model.City;
import model.Measurement;
import model.Node;
import utils.StatUtil;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class CityController {
    private final City city;

    public CityController(City city){
        this.city = city;
    }

    public Map<UUID, Point> getCityState(){
        HashMap <UUID, Point> citySnapshot = new HashMap<>();
        for (Node node : city.getNodesMap().values()){
            citySnapshot.put(node.getUuid(), node.getPosition());
        }
        return citySnapshot;
    }

    public List<Map.Entry<Long, Measurement>> getCityMeasurements(int n){
        TreeMap<Long, Measurement> result = new TreeMap<>();
        city.getMeasurementsMap().forEach((type, treeMap) ->
                treeMap.descendingMap().entrySet().stream().limit(n).forEach(entry ->
                        result.put(entry.getKey(), entry.getValue())));

        return result.descendingMap().entrySet().stream().limit(n).collect(Collectors.toList());
    }

    public List<Map.Entry<Long, Measurement>> getCityMeasurements(String type, int n){
        return city.getMeasurementsMap(type).descendingMap().entrySet().stream().limit(n).collect(Collectors.toList());
    }

    public double getCityMean(String type, int n){
        ArrayList <Double> measurements = new ArrayList<>();
        city.getMeasurementsMap(type).descendingMap().entrySet().stream().limit(n)
                .forEach(e -> measurements.add(e.getValue().getValue()));
        return StatUtil.getMean(measurements);
    }

    public double getCityStandardDeviation(String type, int n){
        ArrayList<Double> measurements = new ArrayList<>();
        city.getMeasurementsMap(type).descendingMap().entrySet().stream().limit(n)
                .forEach(e -> measurements.add(e.getValue().getValue()));
        return Math.sqrt(StatUtil.getVariance(measurements));
    }

    public Map<String, Double> getCityMeans(int n) {
        Map<String, Double> result = new HashMap<>();
        city.getMeasurementsMap().forEach((type, tree) -> {
            ArrayList <Double> measurements = new ArrayList<>();
            tree.descendingMap().entrySet().stream().limit(n).forEach(e ->
                    measurements.add(e.getValue().getValue())
            );
            result.put(type, StatUtil.getMean(measurements));
        });
        return result;
    }

    public Map<String, Double> getCityStandardDeviations(int n){
        Map<String, Double> result = new HashMap<>();
        city.getMeasurementsMap().forEach((type, tree) -> {
            ArrayList <Double> measurements = new ArrayList<>();
            tree.descendingMap().entrySet().stream().limit(n).forEach(e ->
                    measurements.add(e.getValue().getValue())
            );
            result.put(type, Math.sqrt(StatUtil.getVariance(measurements)));
        });
        return result;
    }

    public boolean newCityMeasurement(UUID uuid, Measurement measurement){
        if (isUuidNew(uuid)) return false;
        city.addMeasurement(measurement);
        return true;
    }

    public boolean postGlobalMeasurements(Map<String, Measurement> map) {
        map.forEach((type, measurement) -> city.addMeasurement(measurement));
        return true;
    }

    // Private methods
    private boolean isUuidNew(UUID uuid) {
        return !city.getNodesMap().containsKey(uuid);
    }

}
