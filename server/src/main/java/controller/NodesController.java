package controller;

import model.City;
import model.Measurement;
import model.Node;
import utils.StatUtil;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static java.lang.Math.abs;

public class NodesController {

    private final City city;

    public NodesController(City city) {
        this.city = city;
    }

    public int addNode(Node newNode){
        if (!isUuidNew(newNode.getUuid()))
            return 3;
        if (!isPositionValid(newNode.getPosition()))
            return 2;
        if (!isAreaAroundFree(newNode.getPosition()))
            return 1;
        city.addNode(newNode);
        return 0;
    }

    public int deleteNode(UUID uuid) {
        if (city.removeNode(uuid) == null) {
            return 1;
        }
        city.removeNode(uuid);
        return 0;
    }

    public Map<UUID, Node> getNodesMap(){
        return city.getNodesMap();
    }

    // Analytics

    public Map<UUID, List<Entry<Long, Measurement>>> getLocalMeasurements(int n){

        Map<UUID, List<Entry<Long, Measurement>>> result = new HashMap<>();
        city.getNodesMap().forEach((uuid, node) -> {

            TreeMap<Long, Measurement> temp = new TreeMap<>();
            node.getMeasurements().forEach((type, treeMap) ->
                    treeMap
                            .descendingMap()
                            .entrySet()
                            .stream()
                            .limit(n)
                            .forEach(entry -> temp.put(entry.getKey(), entry.getValue())));

            temp.forEach((timestamp, measurement) ->
                    result.put(
                            uuid,
                            temp
                                    .descendingMap()
                                    .entrySet()
                                    .stream()
                                    .limit(n)
                                    .collect(Collectors.toList())));
        });
        return result;
    }

    public Map<UUID, List<Entry<Long, Measurement>>> getLocalMeasurements(String type, int n){
        Map<UUID, List<Entry<Long, Measurement>>> result = new HashMap<>();
        city.getNodesMap().forEach((uuid, node) ->
                result.put(
                        uuid,
                        node
                                .getMeasurements(type)
                                .descendingMap()
                                .entrySet()
                                .stream()
                                .limit(n)
                                .collect(Collectors.toList())));
        return result;
    }

    public List<Entry<Long, Measurement>> getNodeMeasurements(UUID uuid, int n){
        TreeMap<Long, Measurement> result = new TreeMap<>();
        city.getNodesMap().get(uuid).getMeasurements().forEach((type, treeMap) ->
                treeMap
                        .descendingMap()
                        .entrySet()
                        .stream()
                        .limit(n)
                        .forEach(entry ->
                                result.put(
                                        entry.getKey(),
                                        entry.getValue())));
        return result
                .descendingMap()
                .entrySet()
                .stream()
                .limit(n)
                .collect(Collectors.toList());
    }

    public List<Entry<Long, Measurement>> getNodeMeasurements(UUID uuid, String type, int n){
        return city.getNodesMap()
                .get(uuid)
                .getMeasurements(type)
                .descendingMap()
                .entrySet()
                .stream()
                .limit(n)
                .collect(Collectors.toList());
    }

    public Map<UUID, Map<String, List<Entry<Long, Measurement>>>> getAllTypesLocalMeasurements(int n){

        Map<UUID, Map<String, List<Entry<Long, Measurement>>>> result = new HashMap<>();
        city.getNodesMap().forEach((uuid, node) -> {

            Map<String, List<Entry<Long, Measurement>>> temp = new HashMap<>();
            node.getMeasurements().forEach((type, treeMap) ->
                    temp.put(
                            type,
                            treeMap
                                    .descendingMap()
                                    .entrySet()
                                    .stream()
                                    .limit(n)
                                    .collect(Collectors.toList())));
            result.put(uuid, temp);
        });
        return result;
    }

    public Map<String, List<Entry<Long, Measurement>>> getAllTypesNodeMeasurements (UUID uuid, int n){
        Map<String, List<Entry<Long, Measurement>>> result = new HashMap<>();
        city.getNodesMap().get(uuid).getMeasurements().forEach((type, treeMap) ->
                result.put(
                        type,
                        treeMap
                                .descendingMap()
                                .entrySet()
                                .stream()
                                .limit(n)
                                .collect(Collectors.toList()))
        );
        return result;
    }

    public Map<String, Double> getNodeMeans(UUID uuid, int n){
        Map<String, Double> result = new HashMap<>();
        city.getNodesMap().get(uuid).getMeasurements().forEach((type, treeMap) -> {

            ArrayList <Double> measurements = new ArrayList<>();
            treeMap
                    .descendingMap()
                    .entrySet()
                    .stream()
                    .limit(n)
                    .forEach(entry -> measurements.add(entry.getValue().getValue()));

            result.put(type, StatUtil.getMean(measurements));
        });
        return result;
    }

    public Map<String, Double> getNodeMeans(UUID uuid, String type, int n){
        ArrayList <Double> measurements = new ArrayList<>();
        city.getNodesMap().get(uuid).getMeasurements(type)
                .descendingMap()
                .entrySet()
                .stream()
                .limit(n)
                .forEach(entry -> measurements.add(entry.getValue().getValue()));

        Map<String, Double> result = new HashMap<>();
        result.put(type, StatUtil.getMean(measurements));
        return result;
    }

    public Map<String, Double> getNodeStandardDeviations(UUID uuid, int n){
        Map<String, Double> result = new HashMap<>();
        city.getNodesMap().get(uuid).getMeasurements().forEach((type, treeMap) -> {

            ArrayList<Double> measurements = new ArrayList<>();
            treeMap
                    .descendingMap()
                    .entrySet()
                    .stream()
                    .limit(n)
                    .forEach(entry -> measurements.add(entry.getValue().getValue()));
            result.put(type, Math.sqrt(StatUtil.getVariance(measurements)));
        });
        return result;
    }

    public Map<String, Double> getNodeStandardDeviations(UUID uuid, String type, int n){
        ArrayList <Double> measurements = new ArrayList<>();
        city.getNodesMap().get(uuid).getMeasurements(type)
                .descendingMap()
                .entrySet()
                .stream()
                .limit(n)
                .forEach(entry -> measurements.add(entry.getValue().getValue()));

        Map<String, Double> result = new HashMap<>();
        result.put(type, Math.sqrt(StatUtil.getVariance(measurements)));
        return result;
    }

    public boolean newNodeMeasurement(UUID uuid, Measurement measurement){
        if (!city.getNodesMap().containsKey(uuid))
            return false;
        city.getNodesMap().get(uuid).addMeasurement(measurement);
        return true;
    }

    public boolean postMeasurements(Set<Measurement> measurements){

        measurements.forEach((measurement) -> {

            UUID uuid = measurement.getUuid();
            Node node =  city.getNodesMap().get(uuid);

            if (node != null)
                node.addMeasurement(measurement);

        });

        return true;
    }

    // Private methods
    private boolean isPositionValid(Point position) {
        int x = position.x;
        int y = position.y;
        return (x >= 0 && x < city.getWidth() && y >= 0 && y < city.getHeight());
    }

    private boolean isAreaAroundFree(Point position){
        for (Node node : city.getNodesMap().values()){
            int w = abs(position.x - node.getPosition().x);
            int h = abs(position.y - node.getPosition().y);
            if ((w + h) < city.getMinDistance())
                return false;
        }
        return true;
    }

    private boolean isUuidNew(UUID uuid){
        return (!city.getNodesMap().containsKey(uuid));
    }
}