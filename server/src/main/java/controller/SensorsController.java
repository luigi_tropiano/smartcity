package controller;

import model.City;
import model.Node;

import java.awt.*;

import static java.lang.Math.abs;

public class SensorsController {

    private final City city;

    public SensorsController(City city) {
        this.city = city;
    }

    public Node getNearestNode (Point position){

        Node nearest = null;
        double minDistance = Double.POSITIVE_INFINITY;

        for (Node node : city.getNodesMap().values()){
            if (getDistance(position, node.getPosition()) <= minDistance) {
                minDistance = getDistance(position, node.getPosition());
                nearest = node;
            }
        }
        return nearest;
    }

    private double getDistance(Point firstPoint, Point secondPoint){
        return (abs(firstPoint.x - secondPoint.x) + abs(firstPoint.y - secondPoint.y));
    }
}
