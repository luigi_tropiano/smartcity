package model;

import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.Assert.*;
import static utils.RandomMeasurement.randomMeasurement;
import static utils.RandomNode.randomNode;

public class CityTest {
    private City city;
    private int trials = 5000;

    @Before
    public void setUp() throws Exception {
        city = new City(new Dimension(100, 100));
    }

    @Test
    public void getUuidNodeMap () {
        assertNotNull(city.getNodesMap());
        Node node = randomNode(city.getDimension(), false);
        city.getNodesMap().put(node.getUuid(), node);
        assertTrue(city.getNodesMap().isEmpty());
    }

    @Test
    public void getMeasurementsMap () {
        assertNotNull(city.getMeasurementsMap());
        Measurement measurement = randomMeasurement();
        city.getMeasurementsMap(measurement.getType()).put(measurement.getTimestamp(), measurement);
        assertFalse(city.getMeasurementsMap().isEmpty());
        assertTrue(city.getMeasurementsMap(measurement.getType()).isEmpty());
    }

    @Test
    public void addNode() {
        int bound = trials;
        for (int i = 0; i < bound; i++) {
            Node node = randomNode(city.getDimension(), false);
                assertNull(city.addNode(node));
                assertTrue(city.getNodesMap().containsKey(node.getUuid()));
                assertTrue(city.getNodesMap().containsValue(node));
        }
    }

    @Test
    public void removeNode () {
        int bound = trials;
        ArrayList<UUID> list= new ArrayList<>();
        for (int i = 0; i < bound; i++) {
            Node node = randomNode(city.getDimension(), false);
            city.addNode(node);
            list.add(node.getUuid());
        }
        for (UUID uuid : list) {
            Node removed = city.removeNode(uuid);
            assertEquals(removed.getUuid(), uuid);
            assertFalse(city.getNodesMap().containsKey(uuid));
            assertFalse(city.getNodesMap().containsValue(removed));
        }
    }

    @Test
    public void addMeasurement() {
        Measurement measurement = randomMeasurement();
        city.addMeasurement(measurement);
        assertFalse(city.getMeasurementsMap().isEmpty());
    }
}
