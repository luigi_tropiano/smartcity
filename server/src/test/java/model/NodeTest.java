package model;

import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.net.URI;

import static org.junit.Assert.*;
import static utils.RandomMeasurement.randomMeasurement;
import static utils.RandomNode.randomNode;

public class NodeTest {
    private Node node;

    @Before
    public void setUp() throws Exception {
       node = randomNode(new Dimension(100, 100), true);
    }

    @Test
    public void getUuid() {
        assertNotNull(node.getUuid());
    }

    @Test
    public void setPosition() {
        Point newPosition = new Point(20, 18);
        node.setPosition(newPosition);
        assertEquals(20, node.getPosition().x);
        assertEquals(18, node.getPosition().y);
    }

    @Test
    public void setAddress() {
        URI newAddress = URI.create("http://google.it");
        node.setAddress(newAddress);
        assertEquals(newAddress, node.getAddress());
    }

    @Test
    public void setSensorPort() {
        node.setAnalyticsPort(1234);
        assertEquals(1234, node.getAnalyticsPort());
    }

    @Test
    public void setFederationPort() {
        node.setFederationPort(4321);
        assertEquals(4321, node.getFederationPort());
    }

    @Test
    public void getMeasurements() {
        assertNotNull(node.getMeasurements());
    }

    @Test
    public void addMeasurement() {
        Measurement measurement = randomMeasurement();
        assertTrue(node.getMeasurements().isEmpty());
        for (int i = 0; i < Math.random() * 1000; i++)
            node.addMeasurement(randomMeasurement());
        node.addMeasurement(measurement);
        for (int i = 0; i < Math.random() * 1000; i++)
            node.addMeasurement(randomMeasurement());
        assertFalse(node.getMeasurements().isEmpty());
        assertEquals(node.getMeasurements(measurement.getType()).get(measurement.getTimestamp()), measurement);
        assertEquals(node.getMeasurements(measurement.getType()).get(measurement.getTimestamp()).getTimestamp(), measurement.getTimestamp());
        assertEquals(node.getMeasurements(measurement.getType()).get(measurement.getTimestamp()).getValue(), measurement.getValue(), 0.0);
        assertEquals(node.getMeasurements(measurement.getType()).get(measurement.getTimestamp()).getType(), measurement.getType());
        //node.getMeasurements().descendingMap().forEach((e,f) -> System.out.println(e));
    }

}