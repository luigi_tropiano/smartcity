package controller;

import model.City;
import org.junit.Before;

import java.awt.*;

import static java.lang.Math.abs;
import static utils.RandomCity.randomCity;

public class SensorsControllerTest {
    private City city;
    private SensorsController sensorsController;

    @Before
    public void setUp() throws Exception {
        city = randomCity(new Dimension(100, 100));
        sensorsController = new SensorsController(city);
    }

    //@Test
    //public void getNearestNode() {
    //    Point point = new Point(50, 50);
    //    Node nearest = sensorsController.getNearestNode(point);
    //    for (Node otherNode : city.getNodesMap().values()) {
    //        if (otherNode != nearest)
    //            assertTrue(getDistance(point, nearest.getPosition())
    //                    <= getDistance(point, otherNode.getPosition()));
    //    }
    //}
    private double getDistance(Point firstPoint, Point secondPoint){
        return (abs(firstPoint.x - secondPoint.x) + abs(firstPoint.y - secondPoint.y));
    }
}