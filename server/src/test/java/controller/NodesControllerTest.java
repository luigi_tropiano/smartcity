package controller;

import model.City;
import model.Measurement;
import model.Node;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.Map;
import java.util.TreeMap;

import static java.lang.Double.NaN;
import static org.junit.Assert.*;
import static utils.RandomCity.randomCity;
import static utils.RandomMeasurement.randomMeasurement;
import static utils.RandomNode.randomNode;

public class NodesControllerTest {
    private City city;
    private Dimension cityDimension;
    private NodesController nodesController;
    private int loops = 5000;
    private static String TYPE = "type";

    @Before
    public void setUp() throws Exception {
        cityDimension = new Dimension(100, 100);
        city = randomCity(cityDimension);
        nodesController = new NodesController(city);
    }

    @Test
    public void addNode() {
        city = new City(cityDimension);
        nodesController = new NodesController(city);
        for (int i = 0; i < loops; i++) {
            Node node = randomNode(city.getDimension(), false);
            if (nodesController.addNode(node) == 0) {
                assertTrue(city.getNodesMap().containsKey(node.getUuid()));
                assertEquals(city.getNodesMap().get(node.getUuid()), node);
            } else {
                assertFalse(city.getNodesMap().containsKey(node.getUuid()));
                assertNotEquals(city.getNodesMap().get(node.getUuid()), node);
            }
        }
        assertNotEquals(0, city.getNodesMap().size());
        System.out.println(city.getNodesMap().size());
    }

    @Test
    public void addNodeMax() {
        city = new City(cityDimension);
        nodesController = new NodesController(city);
        for (int i = 0; i < 100; i++){
            for (int j = 0; j < 100; j++){
                Node node = randomNode(new Point(i,j));
                nodesController.addNode(node);
            }
        }
        assertEquals(55, city.getNodesMap().size());
    }

    @Test
    public void deleteNode() {
        assertFalse(city.getNodesMap().isEmpty());
        for (Node node : city.getNodesMap().values()){
            assertTrue(nodesController.deleteNode(node.getUuid()) < 3);
            assertFalse(city.getNodesMap().containsKey(node.getUuid()));
        }
        assertTrue(city.getNodesMap().isEmpty());
    }

    @Test
    public void newNodeMeasurement() {
        for (Node node : city.getNodesMap().values()){
            int n = (int)(Math.random() * 100);
            for (int i = 0; i < n; i ++) {
                Measurement measurement = randomMeasurement();
                nodesController.newNodeMeasurement(node.getUuid(), measurement);
                assertTrue(node.getMeasurements(measurement.getType()).containsKey(measurement.getTimestamp()));
                assertTrue(node.getMeasurements(measurement.getType()).containsValue(measurement));
            }
            assertFalse(node.getMeasurements().isEmpty());
        }
        // test con nodo non presente nella città
        Node node = randomNode(city.getDimension(), true);
        Measurement measurement = randomMeasurement();
        nodesController.newNodeMeasurement(node.getUuid(), measurement);
        assertFalse(node.getMeasurements(measurement.getType()).containsKey(measurement.getTimestamp()));
        assertFalse(node.getMeasurements(measurement.getType()).containsValue(measurement));
    }

    @Test
    public void getNodeMeasurements() {
        for (Node node : city.getNodesMap().values()) {
            for (Map.Entry<String, TreeMap<Long, Measurement>> entry : node.getMeasurements().entrySet()) {
                int n = entry.getValue().size();
                assertEquals(n, nodesController.getNodeMeasurements(node.getUuid(), entry.getKey(), n).size());
            }
        }
    }

    @Test
    public void getLocalMeasurements() {
        for (Node node : city.getNodesMap().values()) {
            for (Map.Entry<String, TreeMap<Long, Measurement>> entry : node.getMeasurements().entrySet()) {
                int n = entry.getValue().size();
                assertEquals(n, nodesController.getLocalMeasurements(entry.getKey(), n).get(node.getUuid()).size());
            }
        }
    }

    @Test
    public void getNodeMeans() {
        city = new City(cityDimension);
        nodesController = new NodesController(city);
        Node node = randomNode(city.getDimension(), true);
        nodesController.addNode(node);
        nodesController.newNodeMeasurement(node.getUuid(), new Measurement(
                node.getUuid(), TYPE,  30,  100000000));
        nodesController.newNodeMeasurement(node.getUuid(), new Measurement(
                node.getUuid(), TYPE,  20,  200000000));
        nodesController.newNodeMeasurement(node.getUuid(), new Measurement(
                node.getUuid(), TYPE,  10,  300000000));
        assertEquals(10, nodesController.getNodeMeans(node.getUuid(), 1).get(TYPE), 0);
        assertEquals(15, nodesController.getNodeMeans(node.getUuid(), 2).get(TYPE), 0);
        assertEquals(20, nodesController.getNodeMeans(node.getUuid(), 3).get(TYPE), 0);
        assertEquals(20, nodesController.getNodeMeans(node.getUuid(), 4).get(TYPE), 0);
    }

    @Test
    public void getNodeStandardDeviations() {
        City city = new City(cityDimension);
        NodesController nodesController = new NodesController(city);
        Node node = randomNode(city.getDimension(), true);
        nodesController.addNode(node);
        nodesController.newNodeMeasurement(node.getUuid(), new Measurement(
                node.getUuid(), TYPE,  30,  100000000));
        nodesController.newNodeMeasurement(node.getUuid(), new Measurement(
                node.getUuid(), TYPE,  20,  200000000));
        nodesController.newNodeMeasurement(node.getUuid(), new Measurement(
                node.getUuid(), TYPE,  10,  300000000));
        assertEquals(NaN, nodesController.getNodeStandardDeviations(node.getUuid(), 1).get(TYPE), 0);
        assertEquals(Math.sqrt(50), nodesController.getNodeStandardDeviations(node.getUuid(), 2).get(TYPE), 0);
        assertEquals(10, nodesController.getNodeStandardDeviations(node.getUuid(), 3).get(TYPE), 0);
        assertEquals(10, nodesController.getNodeStandardDeviations(node.getUuid(), 4).get(TYPE), 0);
    }

}