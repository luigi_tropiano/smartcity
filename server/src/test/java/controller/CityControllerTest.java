package controller;

import model.City;
import model.Measurement;
import model.Node;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.Map;
import java.util.UUID;

import static java.lang.Double.NaN;
import static org.junit.Assert.*;
import static utils.RandomCity.randomCity;
import static utils.RandomMeasurement.randomMeasurement;
import static utils.RandomNode.randomNode;

public class CityControllerTest {
    private City city;
    private Dimension cityDimension;
    private CityController cityController;
    private NodesController nodesController;
    private int loops = 5000;

    @Before
    public void setUp() throws Exception {
        cityDimension = new Dimension(100, 100);
        city = randomCity(cityDimension);
        cityController = new CityController(city);
        nodesController = new NodesController(city);
    }

    @Test
    public void getCityState() {
        for (Node node : city.getNodesMap().values()) {
            assertTrue(cityController.getCityState().containsKey(node.getUuid()));
            assertEquals(cityController.getCityState().get(node.getUuid()), node.getPosition());
        }
    }

    @Test
    public void getCityMeasurements() {
        String type = "type";
        int n = city.getMeasurementsMap(type).size();
        for (Map.Entry<Long, Measurement> entry : city.getMeasurementsMap(type).entrySet()){
            assertEquals(n, cityController.getCityMeasurements(type, n).size());
            assertTrue(cityController.getCityMeasurements(type, n).contains(entry));
        }
    }

    @Test
    public void newCityMeasurement() {
        for (int i = 0; i < loops; i++) {
            Node node = randomNode(city.getDimension(), false);
            Measurement measurement = randomMeasurement();
            if (nodesController.addNode(node) > 0) {
                assertFalse(cityController.newCityMeasurement(node.getUuid(), measurement));
                assertFalse(city.getMeasurementsMap(measurement.getType()).containsKey(measurement.getTimestamp()));
            } else {
                assertTrue(cityController.newCityMeasurement(node.getUuid(), measurement));
                assertTrue(city.getMeasurementsMap(measurement.getType()).containsKey(measurement.getTimestamp()));
            }
        }
    }

    @Test
    public void getCityPm10StandardDeviation() {
        String type = "type";
        City city = new City(cityDimension);
        CityController cityController = new CityController(city);
        NodesController nodesController = new NodesController(city);
        nodesController.addNode(randomNode(city.getDimension(), true));
        UUID uuid = city.getNodesMap().keySet().iterator().next();
        cityController.newCityMeasurement(uuid, new Measurement(
                uuid, type,  30,  100000000));
        cityController.newCityMeasurement(uuid, new Measurement(
                uuid, type,  20,  200000000));
        cityController.newCityMeasurement(uuid, new Measurement(
                uuid, type,  10,  300000000));
        assertEquals(NaN, cityController.getCityStandardDeviation(type, 1), 0);
        assertEquals(Math.sqrt(50), cityController.getCityStandardDeviation(type, 2), 0);
        assertEquals(10, cityController.getCityStandardDeviation(type, 3), 0);
        assertEquals(10, cityController.getCityStandardDeviation(type, 4), 0);
    }

    @Test
    public void getCityPm10Mean() {
        String type = "type";
        City city = new City(cityDimension);
        CityController cityController = new CityController(city);
        NodesController nodesController = new NodesController(city);
        nodesController.addNode(randomNode(city.getDimension(), true));
        UUID uuid = city.getNodesMap().keySet().iterator().next();
        cityController.newCityMeasurement(uuid, new Measurement(
                uuid, type,  30,  100000000));
        cityController.newCityMeasurement(uuid, new Measurement(
                uuid, type,  20,  200000000));
        cityController.newCityMeasurement(uuid, new Measurement(
                uuid, type,  10,  300000000));
        assertEquals(10, cityController.getCityMean(type, 1), 0);
        assertEquals(15, cityController.getCityMean(type, 2), 0);
        assertEquals(20, cityController.getCityMean(type, 3), 0);
        assertEquals(20, cityController.getCityMean(type, 4), 0);
    }
}