package utils;

import controller.CityController;
import controller.NodesController;
import model.City;
import model.Node;

import java.awt.*;

import static utils.RandomMeasurement.randomMeasurement;
import static utils.RandomNode.randomNode;

public class RandomCity {
    private static int loops = 5000;

    private RandomCity(){}

    public static City randomCity(Dimension dimension){
        City city = new City(dimension);
        NodesController nodesController = new NodesController(city);
        CityController cityController = new CityController(city);
        for (int i = 0; i < loops; i++){
            Node node = randomNode(city.getDimension(), false);
            nodesController.addNode(node);
            cityController.newCityMeasurement(node.getUuid(), randomMeasurement());
        }
        for (Node node : city.getNodesMap().values()){
            int n = (int)(Math.random() * 100);
            for  (int i = 0 ; i < n ; i ++)
                node.addMeasurement(randomMeasurement());
        }
        return city;
    }
}
