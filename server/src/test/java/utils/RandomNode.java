package utils;

import model.Node;

import java.awt.*;
import java.net.URI;
import java.util.UUID;

public class RandomNode {

    private RandomNode(){}

    public static Node randomNode(Point position){
        UUID uuid = UUID.randomUUID();
        int sensorPort =  randomPort();
        int federationPort =  randomPort();
        URI address = URI.create("localhost");
        return new Node(uuid, position, address, sensorPort, federationPort);
    }

    public static Node randomNode(Dimension citySize, boolean forceValidPosition){
        UUID uuid = UUID.randomUUID();
        Point position = randomPosition(citySize, forceValidPosition);
        int sensorPort =  randomPort();
        int federationPort =  randomPort();
        URI address = URI.create("localhost");
        return new Node(uuid, position, address, sensorPort, federationPort);
    }

    public static Point randomPosition(Dimension citySize, boolean forceValidPosition){
        int factor = 2;
        if (forceValidPosition) factor = 1;
        int width = (citySize.width - 1) * factor;
        int height = (citySize.width - 1) * factor;
        return new Point((int)(Math.random() * (width)), (int)(Math.random() * (height)));
    }

    public static int randomPort(){
        return (int) (Math.random() * 10000);
    }
}
