package utils;

import model.Measurement;

import java.util.UUID;

public class RandomMeasurement {

    private RandomMeasurement(){}

    public static Measurement randomMeasurement() {
        return new Measurement(UUID.randomUUID(), "type",  Math.random() * 10,
                (long) (System.currentTimeMillis() * Math.random()));
    }
}
