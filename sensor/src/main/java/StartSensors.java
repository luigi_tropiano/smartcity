import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import controller.SensorThread;
import utils.Args;

import java.awt.*;
import java.net.URI;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class StartSensors {
    static { System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$-7s] %5$s %n"); }
    private static final Logger LOGGER = Logger.getLogger("sensor");
    private static final String DEFAULT_SERVER = "http://localhost:8080";

    public static void main(String[] argv) {
        URI serverAddress;
        Integer threads;
        Level logLevel;

        Args args = new Args();

        try {
            JCommander.newBuilder().addObject(args).build().parse(argv);

            threads = args.getThreads();
            logLevel = args.getLogLevel();
            serverAddress = args.getServerAddress();

            if (logLevel == null)
                Arrays.stream(LogManager.getLogManager().getLogger("").getHandlers()).forEach(h -> h.setLevel(Level.WARNING));
            else
                Arrays.stream(LogManager.getLogManager().getLogger("").getHandlers()).forEach(h -> h.setLevel(logLevel));

            if (serverAddress == null) {
                serverAddress = URI.create(DEFAULT_SERVER);
                LOGGER.warning("[BOOT] -> using default server address: " + serverAddress);
            }

            for (int i = 0; i < threads; i++) {

                Point position = randomPosition(new Dimension(100, 100));
                SensorThread sensorThread = new SensorThread(serverAddress, position);
                sensorThread.start();
                LOGGER.info("[BOOT] -> position of sensor " + i + " is ( " + position.x + ", " + position.y + " )");
            }

        }
        catch(ParameterException e){
            //e.getJCommander().setProgramName("");
            LOGGER.severe(e.getMessage());
            e.usage();
        }
    }

    private static Point randomPosition(Dimension citySize){
        int width = (citySize.width - 1);
        int height = (citySize.width - 1);
        return new Point((int)(Math.random() * (width)), (int)(Math.random() * (height)));
    }
}
