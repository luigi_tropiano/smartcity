package utils;

import com.beust.jcommander.IStringConverter;

import java.net.URI;

public class UriConverter implements IStringConverter<URI> {
    @Override
    public URI convert(String value) {
        if (value.contains("http://"))
            return URI.create(value);
        return URI.create("http://" + value);
    }

}
