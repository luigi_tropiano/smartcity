package utils;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.net.URI;
import java.net.URISyntaxException;

public class UriValidator implements IParameterValidator {
    @Override
    public void validate(String name, String value) throws ParameterException {
        try{
            new URI(value);
        }
        catch (URISyntaxException e) {
            throw new ParameterException("Parameter " + name + " should be a valid URI");
        }
    }
}
