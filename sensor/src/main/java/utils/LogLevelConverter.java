package utils;

import com.beust.jcommander.IStringConverter;

import java.util.logging.Level;

public class LogLevelConverter implements IStringConverter<Level> {
    @Override
    public Level convert(String value) {
        switch (value) {
            case "all": return Level.ALL;
            case "info": return Level.INFO;
            case "warning": return Level.WARNING;
            case "severe": return Level.SEVERE;
            case "off": return Level.OFF;
            default: return Level.WARNING;
        }
    }
}
