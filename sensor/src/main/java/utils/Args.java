package utils;

import com.beust.jcommander.Parameter;

import java.net.URI;
import java.util.logging.Level;

public class Args {

    @Parameter(names = {"--threads", "-t"},
            description = "Number of sensors to simulate",
            required = true,
            order = 0)
    private Integer threads;

    @Parameter(names = {"--serverAddress", "-s"},
            description = "Address of the remote server",
            order = 1,
            converter = UriConverter.class,
            validateWith = UriValidator.class)
    private URI serverAddress;

    @Parameter(names = {"--log", "-l"},
            description = "Log level ( all | info | warning | severe | off )",
            order = 3,
            converter = LogLevelConverter.class,
            validateWith = LogLevelValidator.class)
    private Level logLevel;

    @Parameter(names = {"--help", "-h"},
            description = "Show this message",
            order = 4,
            help = true)
    private boolean help = false;

    public Args (){}

    public Integer getThreads() {
        return threads;
    }

    public URI getServerAddress() {
        return serverAddress;
    }

    public Level getLogLevel() {
        return logLevel;
    }

    public boolean isHelp() {
        return help;
    }

}
