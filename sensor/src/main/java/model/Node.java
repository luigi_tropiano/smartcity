package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.unimi.sdp.Commons;

import java.awt.*;
import java.net.URI;
import java.util.UUID;

public class Node {
    private UUID uuid;
    private Point position;
    private URI address;
    private int analyticsPort;
    private int federationPort;

    public Node(Node node) {
        this.uuid = node.uuid;
        this.position = node.position;
        this.address = node.address;
        this.analyticsPort = node.analyticsPort;
        this.federationPort = node.federationPort;
    }

    public Node(Commons.NodeInfo nodeInfo) {
        this.uuid = UUID.fromString(nodeInfo.getUuid());
        this.position = new Point(
                nodeInfo.getPosition().getX(),
                nodeInfo.getPosition().getY());
        this.address = URI.create(nodeInfo.getAddress());
        this.analyticsPort = nodeInfo.getAnalyticsPort();
        this.federationPort = nodeInfo.getFederationPort();
    }

    @JsonCreator
    public Node(@JsonProperty("uuid") UUID uuid,
                @JsonProperty("position") Point position,
                @JsonProperty("address") URI address,
                @JsonProperty("analyticsPort") int analyticsPort,
                @JsonProperty("federationPort") int federationPort) {
        this.uuid = uuid;
        this.position = position;
        this.address = address;
        this.analyticsPort = analyticsPort;
        this.federationPort = federationPort;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public Point getPosition() {
        return new Point(position);
    }

    public void setAddress(URI address) {
        this.address = address;
    }

    public URI getAddress() {
        return address;
    }

    public void setAnalyticsPort(int analyticsPort) {
        this.analyticsPort = analyticsPort;
    }

    public int getAnalyticsPort() {
        return analyticsPort;
    }

    public void setFederationPort(int federationPort) {
        this.federationPort = federationPort;
    }

    public int getFederationPort() {
        return federationPort;
    }

}
