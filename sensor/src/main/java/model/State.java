package model;

import java.awt.*;
import java.net.URI;

public class State {
    private URI serverAddress;
    private Point position;
    private volatile Node nearestNode;
    private volatile boolean stopCondition;
    private volatile long lastStamp;

    public State(URI serverAddress, Point position){
        this.stopCondition = false;
        this.serverAddress = serverAddress;
        this.position = position;
    }

    public URI getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(URI serverAddress) {
        this.serverAddress = serverAddress;
    }

    public Node getNearestNode() {
        return nearestNode;
    }

    public void setNearestNode(Node nearestNode) {
        this.nearestNode = nearestNode;
    }

    public Point getPosition() {
        return position;
    }

    public long getLastStamp() {
        return lastStamp;
    }

    public void setLastStamp(long lastStamp) {
        this.lastStamp = lastStamp;
    }

    public boolean isStopCondition() {
        return stopCondition;
    }

    public void setStopCondition(boolean stopCondition) {
        this.stopCondition = stopCondition;
    }
}
