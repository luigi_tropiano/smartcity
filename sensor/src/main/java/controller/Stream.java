package controller;

import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import it.unimi.sdp.SensorsServiceGrpc.SensorsServiceStub;
import it.unimi.sdp.SensorsServiceOuterClass.LastMeasurement;
import it.unimi.sdp.SensorsServiceOuterClass.MeasurementMessage;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static it.unimi.sdp.SensorsServiceGrpc.newStub;

class Stream {
    private static final Logger LOGGER = Logger.getLogger("sensor");

    private final ManagedChannel channel;
    private final SensorsServiceStub asyncStub;
    private final StreamHandler streamHandler;
    private StreamObserver<MeasurementMessage> streamObserver;

    Stream(ManagedChannel channel, StreamHandler handler){
        this.channel = channel;
        this.streamHandler = handler;
        asyncStub = newStub(channel);
        streamObserver = analyticsStreamObserverBuilder();
    }

    private StreamObserver<MeasurementMessage> analyticsStreamObserverBuilder() {
        return asyncStub.measurementsStream(new StreamObserver<LastMeasurement>() {

            @Override
            public void onNext(LastMeasurement lastMeasurement) {
                System.out.println(lastMeasurement.getTimestamp());
            }

            @Override
            public void onError(Throwable throwable) {
                LOGGER.warning(throwable.getMessage());

                synchronized (streamHandler) {
                    streamHandler.notify();
                }
            }

            @Override
            public void onCompleted() {
                LOGGER.info("[stream] -> completed");
            }
        });
    }

    void shutdown() {
        try {
            if (channel != null) {
                if (!channel.isShutdown()) {
                    streamObserver.onCompleted();
                    channel.shutdown().awaitTermination(3, TimeUnit.SECONDS);
                }
            }
        } catch (InterruptedException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    boolean sendMeasurement(MeasurementMessage message) {

        if (channel.isShutdown())
            return false;

        streamObserver.onNext(message);
        return true;
    }
}
