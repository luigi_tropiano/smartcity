package controller;

import model.Node;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.net.URI;
import java.util.logging.Logger;

@Produces("application/json")
@Consumes(MediaType.APPLICATION_JSON)

public class RestClient {
    private static final Logger LOGGER = Logger.getLogger("sensor");
    private WebTarget webTarget;

    public RestClient(URI uri) {
        webTarget = ClientBuilder.newClient().target(uri);
    }

    public Node getNearestNode(Point position){

        Response response = getNearestNodeResponse(position);

        Node nearestNode = null;
        switch (response.getStatus()) {

            case 200: // OK
                nearestNode = response.readEntity(new GenericType<Node>() {});
                LOGGER.info("nearest node is " + nearestNode.getPosition());
                break;

            case 204: // NO CONTENT
                LOGGER.warning("NO CONTENT");
                break;

            case 400: // BAD REQUEST
                LOGGER.severe("BAD REQUEST");
                break;

            default:
                LOGGER.warning(response.getStatusInfo().toString());
        }
        return nearestNode;
    }

    private Response getNearestNodeResponse(Point position){
        return webTarget
                .path("/nearbynode")
                .queryParam("x", position.x)
                .queryParam("y", position.y)
                .request(MediaType.APPLICATION_JSON)
                .get();
    }
}