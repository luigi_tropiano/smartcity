package controller;

import io.grpc.ManagedChannel;
import model.State;
import simulator.Measurement;
import simulator.SensorStream;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.logging.Logger;

import static it.unimi.sdp.SensorsServiceOuterClass.MeasurementMessage;

class StreamHandler implements SensorStream {
    private static final Logger LOGGER = Logger.getLogger("sensor");

    private State state;
    private Stream stream;
    private final Queue<MeasurementMessage> measurementCache;

    StreamHandler(State state){
        this.state = state;
        this.measurementCache = new ArrayDeque<>();
    }

    void stopStream(){
        if (stream != null) {
            LOGGER.warning("[stream] -> stopping");
            stream.shutdown();
        }
    }

    void newStream(ManagedChannel channel){
        LOGGER.warning("[stream] -> setting :: "
                + state.getNearestNode().getUuid() + " @ " + state.getNearestNode().getAnalyticsPort());
        stream = new Stream(channel, this);
        LOGGER.warning("[stream] -> done");
    }

    @Override
    public void sendMeasurement(Measurement measurement) {
        MeasurementMessage message = MeasurementMessage.newBuilder()
                .setId(measurement.getId())
                .setType(measurement.getType())
                .setValue(measurement.getValue())
                .setTimestamp(measurement.getTimestamp())
                .build();

        if (stream != null)
            stream.sendMeasurement(message);
    }
}
