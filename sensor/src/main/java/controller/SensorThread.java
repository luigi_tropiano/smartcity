package controller;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import it.unimi.sdp.Commons.NodeInfo;
import it.unimi.sdp.Commons.SensorInfo;
import it.unimi.sdp.SensorsServiceOuterClass.Ack;
import it.unimi.sdp.SensorsServiceOuterClass.SensorPing;
import model.Node;
import simulator.PM10Simulator;

import java.awt.*;
import java.net.URI;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static it.unimi.sdp.Commons.SensorInfo.Position;
import static it.unimi.sdp.Commons.SensorInfo.newBuilder;
import static it.unimi.sdp.SensorsServiceGrpc.SensorsServiceBlockingStub;
import static it.unimi.sdp.SensorsServiceGrpc.newBlockingStub;

public class SensorThread extends Thread {
    private static final Logger LOGGER = Logger.getLogger("sensor");

    private final model.State state;
    private final StreamHandler streamHandler;
    private final PM10Simulator pm10Simulator;

    public SensorThread(URI serverAddress, Point position) {
        this.state = new model.State(serverAddress, position);
        this.streamHandler = new StreamHandler(state);
        this.pm10Simulator = new PM10Simulator(streamHandler);
    }

    public void run() {
        try {

            while (!state.isStopCondition()) {

                Node nearestNode = queryServer();

                while (queryNearestNode(nearestNode)) {

                    streamHandler.stopStream();

                    ManagedChannel channel = ManagedChannelBuilder.forAddress(
                            state.getNearestNode().getAddress().toString(),
                            state.getNearestNode().getAnalyticsPort())
                            .usePlaintext().build();

                    state.setLastStamp(askLastStamp(channel));
                    streamHandler.newStream(channel);

                    if (!pm10Simulator.isAlive())
                        pm10Simulator.start();

                    do synchronized (streamHandler) {
                        streamHandler.wait(10000);
                    } while (isStampUnchanged(channel));

                    streamHandler.stopStream();
                }
            }

            pm10Simulator.stopMeGently();
            pm10Simulator.join();

        } catch (InterruptedException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    private synchronized Node queryServer() {
        synchronized (state) {
            RestClient restClient = new RestClient(state.getServerAddress());
            LOGGER.info("[server] -> querying for nearest node");
            Node absoluteNearestNode = restClient.getNearestNode(state.getPosition());
            LOGGER.info("[nearest node] -> " + absoluteNearestNode.getUuid() + " @ " + absoluteNearestNode.getAnalyticsPort());
            return absoluteNearestNode;
        }
    }

    private synchronized boolean queryNearestNode(Node nearestNode) {
        try {

            ManagedChannel channel = ManagedChannelBuilder.forAddress(
                    nearestNode.getAddress().toString(),
                    nearestNode.getAnalyticsPort())
                    .usePlaintext().build();

            SensorsServiceBlockingStub stub = newBlockingStub(channel);

            SensorInfo request = newBuilder()
                    .setPosition(Position.newBuilder()
                            .setX(state.getPosition().x)
                            .setY(state.getPosition().y)
                            .build())
                    .build();

            LOGGER.info("[nearest node] -> querying for nearest leaf");

            NodeInfo response = stub.nearestNode(request);
            Node receivedNode = new Node(response);
            LOGGER.info("[nearest leaf] -> " + receivedNode.getUuid() + " @ " + receivedNode.getAnalyticsPort());

            channel.shutdown().awaitTermination(3, TimeUnit.SECONDS);

            state.setNearestNode(receivedNode);

            return true;

        } catch (StatusRuntimeException e) {
            LOGGER.warning(e.getMessage());
            return false;
        } catch (InterruptedException e) {
            LOGGER.severe(e.getMessage());
            return false;
        }
    }

    private synchronized boolean isStampUnchanged(ManagedChannel channel) {
        try {

            return state.getLastStamp() == askLastStamp(channel);

        } catch (StatusRuntimeException e) {
            LOGGER.warning(e.getMessage());
            return false;
        }
    }

    private synchronized long askLastStamp(ManagedChannel channel) {
        SensorsServiceBlockingStub stub = newBlockingStub(channel);
        Ack response = stub.check(SensorPing.newBuilder().build());
        return response.getStamp();
    }

}
